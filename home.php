<?php
	/*
	Template Name: Home Page
	*/
?>
<?php get_header(); ?>
<!DOCTYPE html>
<html lang="vi">

<!-- Mirrored from sbshouse.vn/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Aug 2023 14:12:59 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
	<title>SBS HOUSE - Công ty TNHH Thiết kế và Xây dựng SBS Đà Nẵng</title>
	<meta name='robots' content='max-image-preview:large' />


<meta name="description" content="SBS HOUSE thiết kế nhà đẹp Đà Nẵng - Quảng Nam. Chuyên thiết kế xây dựng nhà đẹp, thiết kế kiến trúc, thiết kế nội thất, thi công trọn gói nhà phố."/>
<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
<link rel="canonical" href="index.html" />
<meta property="og:locale" content="vi_VN" />
<meta property="og:type" content="website" />
<meta property="og:title" content="SBS HOUSE - Công ty TNHH Thiết kế và Xây dựng SBS Đà Nẵng" />
<meta property="og:description" content="SBS HOUSE thiết kế nhà đẹp Đà Nẵng - Quảng Nam. Chuyên thiết kế xây dựng nhà đẹp, thiết kế kiến trúc, thiết kế nội thất, thi công trọn gói nhà phố." />
<meta property="og:url" content="index.html" />
<meta property="og:site_name" content="Thiết kế &amp; Thi công nhà đẹp tại Đà Nẵng" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="SBS HOUSE thiết kế nhà đẹp Đà Nẵng - Quảng Nam. Chuyên thiết kế xây dựng nhà đẹp, thiết kế kiến trúc, thiết kế nội thất, thi công trọn gói nhà phố." />
<meta name="twitter:title" content="SBS HOUSE - Công ty TNHH Thiết kế và Xây dựng SBS Đà Nẵng" />
<script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://sbshouse.vn/#organization","name":"C\u00f4ng ty TNHH Thi\u1ebft k\u1ebf v\u00e0 X\u00e2y d\u1ef1ng SBS","url":"https://sbshouse.vn/","sameAs":[],"logo":{"@type":"ImageObject","@id":"https://sbshouse.vn/#logo","url":"https://sbshouse.vn/wp-content/uploads/2021/01/logo.png","width":2333,"height":2333,"caption":"C\u00f4ng ty TNHH Thi\u1ebft k\u1ebf v\u00e0 X\u00e2y d\u1ef1ng SBS"},"image":{"@id":"https://sbshouse.vn/#logo"}},{"@type":"WebSite","@id":"https://sbshouse.vn/#website","url":"https://sbshouse.vn/","name":"Thi\u1ebft k\u1ebf &amp; Thi c\u00f4ng nh\u00e0 \u0111\u1eb9p t\u1ea1i \u0110\u00e0 N\u1eb5ng","description":"X\u00e2y Ni\u1ec1m Tin D\u1ef1ng Ch\u1ea5t L\u01b0\u1ee3ng","publisher":{"@id":"https://sbshouse.vn/#organization"},"potentialAction":{"@type":"SearchAction","target":"https://sbshouse.vn/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://sbshouse.vn/#webpage","url":"https://sbshouse.vn/","inLanguage":"vi","name":"SBS HOUSE - C\u00f4ng ty TNHH Thi\u1ebft k\u1ebf v\u00e0 X\u00e2y d\u1ef1ng SBS \u0110\u00e0 N\u1eb5ng","isPartOf":{"@id":"https://sbshouse.vn/#website"},"about":{"@id":"https://sbshouse.vn/#organization"},"datePublished":"2017-05-01T15:51:14+00:00","dateModified":"2022-06-06T21:18:00+00:00","description":"SBS HOUSE thi\u1ebft k\u1ebf nh\u00e0 \u0111\u1eb9p \u0110\u00e0 N\u1eb5ng - Qu\u1ea3ng Nam. Chuy\u00ean thi\u1ebft k\u1ebf x\u00e2y d\u1ef1ng nh\u00e0 \u0111\u1eb9p, thi\u1ebft k\u1ebf ki\u1ebfn tr\u00fac, thi\u1ebft k\u1ebf n\u1ed9i th\u1ea5t, thi c\u00f4ng tr\u1ecdn g\u00f3i nh\u00e0 ph\u1ed1."}]}</script>
<!-- / Yoast SEO Premium plugin. -->

<link rel="alternate" type="application/rss+xml" title="Dòng thông tin Thiết kế &amp; Thi công nhà đẹp tại Đà Nẵng &raquo;" href="feed/index.html" />
<link rel="alternate" type="application/rss+xml" title="Dòng phản hồi Thiết kế &amp; Thi công nhà đẹp tại Đà Nẵng &raquo;" href="comments/feed/index.html" />
<link rel="alternate" type="application/rss+xml" title="Thiết kế &amp; Thi công nhà đẹp tại Đà Nẵng &raquo; Thiết kế thi công nhà đẹp Đà Nẵng &#8211; Quảng Nam Dòng phản hồi" href="sample-page/feed/index.html" />
<script type="text/javascript">
window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/sbshouse.vn\/wp-includes\/js\/wp-emoji-release.min.js?ver=6.2.2"}};
/*! This file is auto-generated */
!function(e,a,t){var n,r,o,i=a.createElement("canvas"),p=i.getContext&&i.getContext("2d");function s(e,t){p.clearRect(0,0,i.width,i.height),p.fillText(e,0,0);e=i.toDataURL();return p.clearRect(0,0,i.width,i.height),p.fillText(t,0,0),e===i.toDataURL()}function c(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(o=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},r=0;r<o.length;r++)t.supports[o[r]]=function(e){if(p&&p.fillText)switch(p.textBaseline="top",p.font="600 32px Arial",e){case"flag":return s("\ud83c\udff3\ufe0f\u200d\u26a7\ufe0f","\ud83c\udff3\ufe0f\u200b\u26a7\ufe0f")?!1:!s("\ud83c\uddfa\ud83c\uddf3","\ud83c\uddfa\u200b\ud83c\uddf3")&&!s("\ud83c\udff4\udb40\udc67\udb40\udc62\udb40\udc65\udb40\udc6e\udb40\udc67\udb40\udc7f","\ud83c\udff4\u200b\udb40\udc67\u200b\udb40\udc62\u200b\udb40\udc65\u200b\udb40\udc6e\u200b\udb40\udc67\u200b\udb40\udc7f");case"emoji":return!s("\ud83e\udef1\ud83c\udffb\u200d\ud83e\udef2\ud83c\udfff","\ud83e\udef1\ud83c\udffb\u200b\ud83e\udef2\ud83c\udfff")}return!1}(o[r]),t.supports.everything=t.supports.everything&&t.supports[o[r]],"flag"!==o[r]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[o[r]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(e=t.source||{}).concatemoji?c(e.concatemoji):e.wpemoji&&e.twemoji&&(c(e.twemoji),c(e.wpemoji)))}(window,document,window._wpemojiSettings);
</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 0.07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='wp-block-library-css' href='wp-includes/css/dist/block-library/style.min3781.css?ver=6.2.2' type='text/css' media='all' />
<link rel='stylesheet' id='classic-theme-styles-css' href='wp-includes/css/classic-themes.min3781.css?ver=6.2.2' type='text/css' media='all' />
<style id='global-styles-inline-css' type='text/css'>
body{--wp--preset--color--black: #000000;--wp--preset--color--cyan-bluish-gray: #abb8c3;--wp--preset--color--white: #ffffff;--wp--preset--color--pale-pink: #f78da7;--wp--preset--color--vivid-red: #cf2e2e;--wp--preset--color--luminous-vivid-orange: #ff6900;--wp--preset--color--luminous-vivid-amber: #fcb900;--wp--preset--color--light-green-cyan: #7bdcb5;--wp--preset--color--vivid-green-cyan: #00d084;--wp--preset--color--pale-cyan-blue: #8ed1fc;--wp--preset--color--vivid-cyan-blue: #0693e3;--wp--preset--color--vivid-purple: #9b51e0;--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg,rgba(6,147,227,1) 0%,rgb(155,81,224) 100%);--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg,rgb(122,220,180) 0%,rgb(0,208,130) 100%);--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg,rgba(252,185,0,1) 0%,rgba(255,105,0,1) 100%);--wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg,rgba(255,105,0,1) 0%,rgb(207,46,46) 100%);--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg,rgb(238,238,238) 0%,rgb(169,184,195) 100%);--wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg,rgb(74,234,220) 0%,rgb(151,120,209) 20%,rgb(207,42,186) 40%,rgb(238,44,130) 60%,rgb(251,105,98) 80%,rgb(254,248,76) 100%);--wp--preset--gradient--blush-light-purple: linear-gradient(135deg,rgb(255,206,236) 0%,rgb(152,150,240) 100%);--wp--preset--gradient--blush-bordeaux: linear-gradient(135deg,rgb(254,205,165) 0%,rgb(254,45,45) 50%,rgb(107,0,62) 100%);--wp--preset--gradient--luminous-dusk: linear-gradient(135deg,rgb(255,203,112) 0%,rgb(199,81,192) 50%,rgb(65,88,208) 100%);--wp--preset--gradient--pale-ocean: linear-gradient(135deg,rgb(255,245,203) 0%,rgb(182,227,212) 50%,rgb(51,167,181) 100%);--wp--preset--gradient--electric-grass: linear-gradient(135deg,rgb(202,248,128) 0%,rgb(113,206,126) 100%);--wp--preset--gradient--midnight: linear-gradient(135deg,rgb(2,3,129) 0%,rgb(40,116,252) 100%);--wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');--wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');--wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');--wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');--wp--preset--duotone--midnight: url('#wp-duotone-midnight');--wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');--wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');--wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');--wp--preset--font-size--small: 13px;--wp--preset--font-size--medium: 20px;--wp--preset--font-size--large: 36px;--wp--preset--font-size--x-large: 42px;--wp--preset--spacing--20: 0.44rem;--wp--preset--spacing--30: 0.67rem;--wp--preset--spacing--40: 1rem;--wp--preset--spacing--50: 1.5rem;--wp--preset--spacing--60: 2.25rem;--wp--preset--spacing--70: 3.38rem;--wp--preset--spacing--80: 5.06rem;--wp--preset--shadow--natural: 6px 6px 9px rgba(0, 0, 0, 0.2);--wp--preset--shadow--deep: 12px 12px 50px rgba(0, 0, 0, 0.4);--wp--preset--shadow--sharp: 6px 6px 0px rgba(0, 0, 0, 0.2);--wp--preset--shadow--outlined: 6px 6px 0px -3px rgba(255, 255, 255, 1), 6px 6px rgba(0, 0, 0, 1);--wp--preset--shadow--crisp: 6px 6px 0px rgba(0, 0, 0, 1);}:where(.is-layout-flex){gap: 0.5em;}body .is-layout-flow > .alignleft{float: left;margin-inline-start: 0;margin-inline-end: 2em;}body .is-layout-flow > .alignright{float: right;margin-inline-start: 2em;margin-inline-end: 0;}body .is-layout-flow > .aligncenter{margin-left: auto !important;margin-right: auto !important;}body .is-layout-constrained > .alignleft{float: left;margin-inline-start: 0;margin-inline-end: 2em;}body .is-layout-constrained > .alignright{float: right;margin-inline-start: 2em;margin-inline-end: 0;}body .is-layout-constrained > .aligncenter{margin-left: auto !important;margin-right: auto !important;}body .is-layout-constrained > :where(:not(.alignleft):not(.alignright):not(.alignfull)){max-width: var(--wp--style--global--content-size);margin-left: auto !important;margin-right: auto !important;}body .is-layout-constrained > .alignwide{max-width: var(--wp--style--global--wide-size);}body .is-layout-flex{display: flex;}body .is-layout-flex{flex-wrap: wrap;align-items: center;}body .is-layout-flex > *{margin: 0;}:where(.wp-block-columns.is-layout-flex){gap: 2em;}.has-black-color{color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-color{color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-color{color: var(--wp--preset--color--white) !important;}.has-pale-pink-color{color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-color{color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-color{color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-color{color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-color{color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-color{color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-color{color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-color{color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-color{color: var(--wp--preset--color--vivid-purple) !important;}.has-black-background-color{background-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-background-color{background-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-background-color{background-color: var(--wp--preset--color--white) !important;}.has-pale-pink-background-color{background-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-background-color{background-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-background-color{background-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-background-color{background-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-background-color{background-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-background-color{background-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-background-color{background-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-background-color{background-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-background-color{background-color: var(--wp--preset--color--vivid-purple) !important;}.has-black-border-color{border-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-border-color{border-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-border-color{border-color: var(--wp--preset--color--white) !important;}.has-pale-pink-border-color{border-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-border-color{border-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-border-color{border-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-border-color{border-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-border-color{border-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-border-color{border-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-border-color{border-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-border-color{border-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-border-color{border-color: var(--wp--preset--color--vivid-purple) !important;}.has-vivid-cyan-blue-to-vivid-purple-gradient-background{background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;}.has-light-green-cyan-to-vivid-green-cyan-gradient-background{background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;}.has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;}.has-luminous-vivid-orange-to-vivid-red-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;}.has-very-light-gray-to-cyan-bluish-gray-gradient-background{background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;}.has-cool-to-warm-spectrum-gradient-background{background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;}.has-blush-light-purple-gradient-background{background: var(--wp--preset--gradient--blush-light-purple) !important;}.has-blush-bordeaux-gradient-background{background: var(--wp--preset--gradient--blush-bordeaux) !important;}.has-luminous-dusk-gradient-background{background: var(--wp--preset--gradient--luminous-dusk) !important;}.has-pale-ocean-gradient-background{background: var(--wp--preset--gradient--pale-ocean) !important;}.has-electric-grass-gradient-background{background: var(--wp--preset--gradient--electric-grass) !important;}.has-midnight-gradient-background{background: var(--wp--preset--gradient--midnight) !important;}.has-small-font-size{font-size: var(--wp--preset--font-size--small) !important;}.has-medium-font-size{font-size: var(--wp--preset--font-size--medium) !important;}.has-large-font-size{font-size: var(--wp--preset--font-size--large) !important;}.has-x-large-font-size{font-size: var(--wp--preset--font-size--x-large) !important;}
.wp-block-navigation a:where(:not(.wp-element-button)){color: inherit;}
:where(.wp-block-columns.is-layout-flex){gap: 2em;}
.wp-block-pullquote{font-size: 1.5em;line-height: 1.6;}
</style>
<link rel='stylesheet' id='contact-form-7-css' href='wp-content/plugins/contact-form-7/includes/css/stylesf2b4.css?ver=5.7.7' type='text/css' media='all' />
<link rel='stylesheet' id='kk-star-ratings-css' href='wp-content/plugins/kk-star-ratings/src/core/public/css/kk-star-ratings.min5243.css?ver=5.4.5' type='text/css' media='all' />
<link rel='stylesheet' id='wp-pagenavi-css' href='wp-content/plugins/wp-pagenavi/pagenavi-css44fd.css?ver=2.70' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap.min-css' href='wp-content/themes/sbshouse/css/bootstrap.mind58c.css?ver=1.1.1111' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css' href='wp-content/plugins/elementor/assets/lib/font-awesome/css/font-awesome.min1849.css?ver=4.7.0' type='text/css' media='all' />
<link rel='stylesheet' id='animate.min-css' href='wp-content/themes/sbshouse/css/animate.mind58c.css?ver=1.1.1111' type='text/css' media='all' />
<link rel='stylesheet' id='owl.carousel-css' href='wp-content/themes/sbshouse/css/owl.carousel3781.css?ver=6.2.2' type='text/css' media='all' />
<link rel='stylesheet' id='default-css' href='wp-content/themes/sbshouse/css/defaultd58c.css?ver=1.1.1111' type='text/css' media='all' />
<link rel='stylesheet' id='theme-css' href='wp-content/themes/sbshouse/css/themed58c.css?ver=1.1.1111' type='text/css' media='all' />
<link rel='stylesheet' id='responsive-css' href='wp-content/themes/sbshouse/css/responsived58c.css?ver=1.1.1111' type='text/css' media='all' />
<link rel='stylesheet' id='dflip-icons-style-css' href='wp-content/plugins/3d-flipbook-dflip-lite/assets/css/themify-icons.min3023.css?ver=1.7.35' type='text/css' media='all' />
<link rel='stylesheet' id='dflip-style-css' href='wp-content/plugins/3d-flipbook-dflip-lite/assets/css/dflip.min3023.css?ver=1.7.35' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-css' href='wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.minfc13.css?ver=5.20.0' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-frontend-css' href='wp-content/plugins/elementor/assets/css/frontend-lite.mina1c3.css?ver=3.14.1' type='text/css' media='all' />
<link rel='stylesheet' id='swiper-css' href='wp-content/plugins/elementor/assets/lib/swiper/v8/css/swiper.min94a4.css?ver=8.4.5' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-38710-css' href='wp-content/uploads/elementor/css/post-3871093de.css?ver=1689312930' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-global-css' href='wp-content/uploads/elementor/css/globalac06.css?ver=1689312931' type='text/css' media='all' />
<link rel='stylesheet' id='google-fonts-1-css' href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;display=swap&amp;subset=vietnamese&amp;ver=6.2.2' type='text/css' media='all' />
<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin><script type='text/javascript' src='wp-includes/js/jquery/jquery.min5aed.js?ver=3.6.4' id='jquery-core-js'></script>
<script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min6b00.js?ver=3.4.0' id='jquery-migrate-js'></script>
<script type='text/javascript' src='wp-content/themes/sbshouse/js/owl.carousel.mind58c.js?ver=1.1.1111' id='owl.carousel.min-js'></script>
<script type='text/javascript' src='wp-content/themes/sbshouse/js/jquery.wookmark.mind58c.js?ver=1.1.1111' id='jquery.wookmark-js'></script>
<script type='text/javascript' src='wp-content/themes/sbshouse/js/maind58c.js?ver=1.1.1111' id='main-js'></script>
<link rel="https://api.w.org/" href="wp-json/index.html" /><link rel="alternate" type="application/json" href="wp-json/wp/v2/pages/14696.json" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.html?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" />
<meta name="generator" content="WordPress 6.2.2" />
<link rel='shortlink' href='index.html' />
<link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embedf91d.json?url=https%3A%2F%2Fsbshouse.vn%2F" />
<link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embed5eb3?url=https%3A%2F%2Fsbshouse.vn%2F&amp;format=xml" />
<script type="application/ld+json">{
    "@context": "https://schema.org/",
    "@type": "CreativeWorkSeries",
    "name": "Thiết kế thi công nhà đẹp Đà Nẵng - Quảng Nam",
    "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "4.8",
        "bestRating": "5",
        "ratingCount": "32"
    }
}</script>    <script type="text/javascript">
    var ajaxurl = "wp-admin/admin-ajax.html";
    var home_url = "index.html";
    var THEME_URL = "wp-content/themes/sbshouse/index.html";
    </script>
        <link href="wp-content/themes/sbshouse/addon/lightGallery/media/css/lightgallery.css" rel="stylesheet">
    <script src="wp-content/themes/sbshouse/addon/lightGallery/media/js/picturefill.min.js"></script>
    <script src="wp-content/themes/sbshouse/addon/lightGallery/media/js/lightgallery.js"></script>
    <script src="wp-content/themes/sbshouse/addon/lightGallery/media/js/lg-fullscreen.js"></script>
    <script src="wp-content/themes/sbshouse/addon/lightGallery/media/js/lg-video.js"></script>
    <script src="wp-content/themes/sbshouse/addon/lightGallery/media/js/lg-autoplay.js"></script>
    <script src="wp-content/themes/sbshouse/addon/lightGallery/media/js/lg-zoom.js"></script>
    <script src="wp-content/themes/sbshouse/addon/lightGallery/media/js/lg-hash.js"></script>
    <script src="wp-content/themes/sbshouse/addon/lightGallery/media/js/lg-pager.js"></script>
    <script src="wp-content/themes/sbshouse/addon/lightGallery/media/js/jquery.mousewheel.min.js"></script>
    <script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.lightGallery-event').lightGallery({'hash':false});
    });
    </script>
    <script data-cfasync="false"> var dFlipLocation = "wp-content/plugins/3d-flipbook-dflip-lite/assets/index.html"; var dFlipWPGlobal = {"text":{"toggleSound":"Turn on\/off Sound","toggleThumbnails":"Toggle Thumbnails","toggleOutline":"Toggle Outline\/Bookmark","previousPage":"Previous Page","nextPage":"Next Page","toggleFullscreen":"Toggle Fullscreen","zoomIn":"Zoom In","zoomOut":"Zoom Out","toggleHelp":"Toggle Help","singlePageMode":"Single Page Mode","doublePageMode":"Double Page Mode","downloadPDFFile":"Download PDF File","gotoFirstPage":"Goto First Page","gotoLastPage":"Goto Last Page","share":"Share","mailSubject":"I wanted you to see this FlipBook","mailBody":"Check out this site {{url}}","loading":"DearFlip: Loading "},"moreControls":"download,pageMode,startPage,endPage,sound","hideControls":"","scrollWheel":"true","backgroundColor":"#777","backgroundImage":"","height":"auto","paddingLeft":"20","paddingRight":"20","controlsPosition":"bottom","duration":800,"soundEnable":"true","enableDownload":"true","enableAnnotation":"false","enableAnalytics":"false","webgl":"true","hard":"none","maxTextureSize":"1600","rangeChunkSize":"524288","zoomRatio":1.5,"stiffness":3,"pageMode":"0","singlePageMode":"0","pageSize":"0","autoPlay":"false","autoPlayDuration":5000,"autoPlayStart":"false","linkTarget":"2","sharePrefix":"dearflip-"};</script><meta name="generator" content="Elementor 3.14.1; features: e_dom_optimization, e_optimized_assets_loading, e_optimized_css_loading, a11y_improvements, additional_custom_breakpoints; settings: css_print_method-external, google_font-enabled, font_display-swap">
<link rel="icon" href="wp-content/uploads/2021/06/cropped-cropped-logo-1-32x32.png" sizes="32x32" />
<link rel="icon" href="wp-content/uploads/2021/06/cropped-cropped-logo-1-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon" href="wp-content/uploads/2021/06/cropped-cropped-logo-1-180x180.png" />
<meta name="msapplication-TileImage" content="https://sbshouse.vn/wp-content/uploads/2021/06/cropped-cropped-logo-1-270x270.png" />
	<link rel="stylesheet" type="text/css" href="wp-content/themes/sbshouse/style.css" media="screen" />
	<link type="image/x-icon" href="wp-content/themes/sbshouse/images/cropped-cropped-logo-1-32x32.png" rel="shortcut icon">
	<link rel="preconnect" href="https://fonts.googleapis.com/">
	<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Bai+Jamjuree:wght@200;300;400;500;600;700&amp;display=swap" rel="stylesheet">
	<style>
	:root {
	    --cl-0: #adadaf;
	    --cl-1: #ffba00;
	    --cl-2: #161616;
	}
	</style>
	<!--Start of Tawk.to Script-->
	<script type="text/javascript">
		
	
	(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/63e7cc67c2f1ac1e2032bb4c/1gp0nct5u';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
	})();
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	window.Tawk_API.onLoad = function(){
	    window.Tawk_API.hideWidget();
	};
	window.Tawk_API.onChatMinimized = function(){
	    window.Tawk_API.hideWidget();
	    jQuery('.tld_chat_box_content').css({"display":"none"});
    	jQuery('.tld_chat_show').show();
	};
	</script>
	<!--End of Tawk.to Script-->
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TQN8VTT');</script>
<!-- End Google Tag Manager -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-123993667-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-123993667-1');
</script>


<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-H9GEBSZL8F"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-H9GEBSZL8F');
</script>

<meta name="facebook-domain-verification" content="pqidsbbp35ka6kzoj0yi7rrbtebolg" />

<!-- Meta Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  '../connect.facebook.net/en_US/fbevents.js');
  fbq('init', '5357605361016454');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=5357605361016454&amp;ev=PageView&amp;noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->
</head>
<body class="home page-template page-template-templates page-template-template-home page-template-templatestemplate-home-php page page-id-14696 elementor-default elementor-kit-38710 elementor-page elementor-page-14696">
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQN8VTT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<header id='sc_header'>
		<div class="container">
			<div class='sc_header_body'>
				<a href='index.html' class='logo'><img src="wp-content/uploads/2022/07/logo.svg"></a>
				<div class='main_menu'>
					<div class='top_menu_mobile'><a class='trigger_menu2'><i class="fa fa-close"></i></a></div>
					<div class="menu-primary-container"><ul id="menu-primary" class="menu"><li id="menu-item-23842" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14696 current_page_item menu-item-23842"><a href="index.html" aria-current="page">TRANG CHỦ</a></li>
<li id="menu-item-14769" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-14769"><a rel="nofollow" href="gioi-thieu/index.html">GIỚI THIỆU</a>
<ul class="sub-menu">
	<li id="menu-item-28041" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28041"><a href="gioi-thieu/index.html">Giới Thiệu</a></li>
	<li id="menu-item-35298" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35298"><a href="sbshouse-profile/index.html">SBS Profile</a></li>
	<li id="menu-item-28092" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28092"><a href="so-do-to-chuc/index.html">Sơ đồ tổ chức</a></li>
	<li id="menu-item-23833" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-23833"><a href="chuyen-muc/sbs-life-style/index.html">SBS Life Style</a></li>
</ul>
</li>
<li id="menu-item-14770" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-14770"><a href="chuyen-muc/dich-vu/index.html">DỊCH VỤ</a>
<ul class="sub-menu">
	<li id="menu-item-14771" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-14771"><a href="cong-ty-thiet-ke-noi-that-da-nang-sbs/index.html">Thiết kế &#038; thi công nội thất</a></li>
	<li id="menu-item-14783" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-14783"><a href="thiet-ke-kien-truc/index.html">Thiết kế kiến trúc</a></li>
	<li id="menu-item-19075" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-19075"><a href="thi-cong-xay-dung/index.html">Thi công phần thô</a></li>
	<li id="menu-item-19076" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-19076"><a href="dich-vu-hoan-thien-nha-xay-tho/index.html">Thi công hoàn thiện</a></li>
	<li id="menu-item-19077" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-19077"><a href="thiet-ke-thi-cong-nha-pho-tron-goi-da-nang/index.html">Xây nhà trọn gói Đà Nẵng</a></li>
	<li id="menu-item-25146" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-25146"><a href="bao-gia-xay-nha-tron-goi-tp-hcm/index.html">Xây nhà trọn gói TP HCM</a></li>
	<li id="menu-item-25915" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-25915"><a href="xay-nha-tron-goi-bien-hoa-dong-nai/index.html">Xây nhà trọn gói Đồng Nai</a></li>
	<li id="menu-item-25945" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-25945"><a href="xay-nha-tron-goi-binh-duong/index.html">Xây nhà trọn gói Bình Dương</a></li>
</ul>
</li>
<li id="menu-item-14788" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-14788"><a href="chuyen-muc/mau-nha-dep/index.html">DỰ ÁN</a>
<ul class="sub-menu">
	<li id="menu-item-19668" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-19668"><a rel="nofollow" href="chuyen-muc/cong-trinh-thi-cong/index.html">Công trình thi công</a>
	<ul class="sub-menu">
		<li id="menu-item-38446" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-38446"><a href="chuyen-muc/khoanh-khac-thi-cong/index.html">Khoảnh khắc thi công</a></li>
	</ul>
</li>
	<li id="menu-item-14789" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-14789"><a href="chuyen-muc/nha-pho-2-tang/index.html">Nhà phố 2 tầng</a></li>
	<li id="menu-item-14790" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-14790"><a href="chuyen-muc/nha-pho-3-tang/index.html">Nhà phố 3 tầng</a></li>
	<li id="menu-item-14791" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-14791"><a href="chuyen-muc/nha-pho-4-tang/index.html">Nhà phố 4 tầng</a></li>
	<li id="menu-item-14794" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-14794"><a href="chuyen-muc/biet-thu-nha-vuon-2/index.html">Biệt thự, Nhà vườn</a>
	<ul class="sub-menu">
		<li id="menu-item-28967" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-28967"><a href="chuyen-muc/biet-thu-2-tang/index.html">Biệt thự 2 tầng</a></li>
		<li id="menu-item-28968" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-28968"><a href="chuyen-muc/biet-thu-3-tang/index.html">Biệt thự 3 tầng</a></li>
	</ul>
</li>
	<li id="menu-item-14797" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-14797"><a rel="nofollow" href="chuyen-muc/nha-cap-4/index.html">Nhà cấp 4 đẹp</a></li>
	<li id="menu-item-14793" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-14793"><a rel="nofollow" href="chuyen-muc/can-ho-nha-tro/index.html">Căn hộ &#8211; Nhà trọ &#8211; Chung cư</a></li>
	<li id="menu-item-14792" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-14792"><a rel="nofollow" href="chuyen-muc/toa-nha-van-phong-khach-san/index.html">Toà nhà văn phòng &#8211; Khách sạn</a></li>
</ul>
</li>
<li id="menu-item-18637" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-18637"><a href="bao-gia-sbs-house/index.html">BÁO GIÁ</a>
<ul class="sub-menu">
	<li id="menu-item-14800" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-14800"><a href="bao-gia-thiet-ke-nha-tai-da-nang/index.html">Báo giá thiết kế</a></li>
	<li id="menu-item-14801" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-14801"><a href="gia-xay-dung-phan-tho-da-nang/index.html">Báo giá xây dựng phần thô</a></li>
	<li id="menu-item-14802" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-14802"><a href="bao-gia-hoan-thien-nha-pho/index.html">Báo giá hoàn thiện</a></li>
</ul>
</li>
<li id="menu-item-14804" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-14804"><a rel="nofollow" href="chuyen-muc/kien-thuc-xay-nha/index.html">KIẾN THỨC</a>
<ul class="sub-menu">
	<li id="menu-item-28325" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-28325"><a href="chuyen-muc/cam-nang-thi-cong/index.html">Cẩm nang thi công SBS</a></li>
	<li id="menu-item-27934" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-27934"><a href="diem-khac-biet-trong-thi-cong-cua-sbs-house/index.html">Khác biệt về thi công SBS</a></li>
	<li id="menu-item-14806" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-14806"><a rel="nofollow" href="chuyen-muc/kien-thuc-xay-nha/xay-dung/index.html">Kiến thức xây dựng</a></li>
	<li id="menu-item-14805" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-14805"><a rel="nofollow" href="chuyen-muc/kien-thuc-xay-nha/phong-thuy/index.html">Kiến thức phong thủy</a></li>
	<li id="menu-item-38939" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-38939"><a href="faqs/index.html">Câu hỏi thường gặp</a></li>
	<li id="menu-item-40322" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-40322"><a href="video-kien-thuc/index.html">Video kiến thức</a></li>
</ul>
</li>
<li id="menu-item-38080" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-38080"><a href="chuyen-muc/sbs-running/index.html">SBS RUNNING</a></li>
<li id="menu-item-25794" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-25794"><a href="chuyen-muc/phan-hoi-cua-khach-hang/index.html">PHẢN HỒI</a></li>
<li id="menu-item-40039" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40039"><a href="sbshouse-tuyen-dung/index.html">TUYỂN DỤNG</a></li>
</ul></div>				</div>
				<div class='sc_header-right'>
					<a class='btn-hotline btn-hotline2'>0972 910 046</a>
					<a class='btn-hotline desktop_hidden' href="tel:0972 910 046">0972 910 046</a>
					<a class='btn-nbgia desktop_hidden' href='tel:0972910046'>Gọi ngay </a>
					<a class='trigger_search'><i class="fa fa-search"></i></a>
					<form class="form_search" role="search" method="get" action="https://sbshouse.vn/">
					    <input  placeholder="Từ khóa tìm kiếm ..." value="" name="s" id="s" >
					    <button><i class="fa fa-search"></i></button>
					</form>
					<a class='bar_ioc'>
						<img src='wp-content/themes/sbshouse/images/icon_bar.png'>
					</a>
					<a class='trigger_menu'>
						<i class="fa fa-bars"></i>
					</a>
				</div>
			</div>
		</div>
	</header><div class='box_video_full'>
	<video id='sc_video' controlls="" playsinline="" muted="" loop>
		<source src="wp-content/uploads/2022/08/sbs-house-video-web-fix-2.mp4" type="video/mp4">
	</video>
</div>
    	<div class='sc_home-slide sc_slide sc_slide1' loop="yes" nav="true" autoplay="yes" animateOut="fadeOut" autoplayTimeout="5000" smartSpeed="500">
    	            <div class='sc_home-slide-item'>
				<img src='wp-content/uploads/2022/06/SLIDE-HOME1.jpg'>
				<div class='opacity'></div>
				<div class='content-box'>
					<div class='title'>CHÚNG TÔI SINH RA VÌ HẠNH PHÚC CỦA CHÍNH BẠN</div>
					<div class='content'>
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">SBS HOUSE là công ty hoạt động toàn diện trong lĩnh vực thiết kế - thi công nhà phố theo phong cách hiện đại tại Đà Nẵng - Quảng Nam. Với hơn 5 năm hoạt động, chúng tôi đã đồng hành cùng hàng ngàn khách hàng để tạo nên những tổ ấm đẹp và mang giá trị bền vững.</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mobile_none">Chúng tôi may mắn sở hữu một đội ngũ kiến trúc sư, kỹ sư vững chuyên môn, giàu kinh nghiệm; hứa hẹn sẽ mang đến những công trình khiến bạn hài lòng nhất. Với phương châm luôn đặt lợi ích của khách hàng lên hàng đầu, mọi sản phẩm mà SBS HOUSE tạo ra đều hướng đến lợi ích của bạn.</div>
						</div>
					</div>
				</div>
			</div>
                        <div class='sc_home-slide-item'>
				<img src='wp-content/uploads/2022/06/SLIDE-HOME2.jpg'>
				<div class='opacity'></div>
				<div class='content-box'>
					<div class='title'>CHÚNG TÔI SINH RA VÌ HẠNH PHÚC CỦA CHÍNH BẠN</div>
					<div class='content'>
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">SBS HOUSE là công ty hoạt động toàn diện trong lĩnh vực thiết kế - thi công nhà phố theo phong cách hiện đại tại Đà Nẵng - Quảng Nam. Với hơn 5 năm hoạt động, chúng tôi đã đồng hành cùng hàng ngàn khách hàng để tạo nên những tổ ấm đẹp và mang giá trị bền vững.</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mobile_none">Chúng tôi may mắn sở hữu một đội ngũ kiến trúc sư, kỹ sư vững chuyên môn, giàu kinh nghiệm; hứa hẹn sẽ mang đến những công trình khiến bạn hài lòng nhất. Với phương châm luôn đặt lợi ích của khách hàng lên hàng đầu, mọi sản phẩm mà SBS HOUSE tạo ra đều hướng đến lợi ích của bạn.</div>
						</div>
					</div>
				</div>
			</div>
                        <div class='sc_home-slide-item'>
				<img src='wp-content/uploads/2022/06/SLIDE-HOME3.jpg'>
				<div class='opacity'></div>
				<div class='content-box'>
					<div class='title'>CHÚNG TÔI SINH RA VÌ HẠNH PHÚC CỦA CHÍNH BẠN</div>
					<div class='content'>
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">SBS HOUSE là công ty hoạt động toàn diện trong lĩnh vực thiết kế - thi công nhà phố theo phong cách hiện đại tại Đà Nẵng - Quảng Nam. Với hơn 5 năm hoạt động, chúng tôi đã đồng hành cùng hàng ngàn khách hàng để tạo nên những tổ ấm đẹp và mang giá trị bền vững.</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mobile_none">Chúng tôi may mắn sở hữu một đội ngũ kiến trúc sư, kỹ sư vững chuyên môn, giàu kinh nghiệm; hứa hẹn sẽ mang đến những công trình khiến bạn hài lòng nhất. Với phương châm luôn đặt lợi ích của khách hàng lên hàng đầu, mọi sản phẩm mà SBS HOUSE tạo ra đều hướng đến lợi ích của bạn.</div>
						</div>
					</div>
				</div>
			</div>
                    </div>
        <div class='sc_home-statics' bgstyle='background-image:url()'>
	<div class='sc_home-statics-n'>
		<div class="container">
			<div class='sc_home-statics-box'>
								            <div class='sc_home-statics-item'>
								<div class='title'>
									<span class='sc_home-statics-item-number number_transtion' data-content='5'>5</span> +								</div>
								<div class='content'>Năm kinh nghiệm</div>
							</div>
				            				            <div class='sc_home-statics-item'>
								<div class='title'>
									<span class='sc_home-statics-item-number number_transtion' data-content='80'>80</span> +								</div>
								<div class='content'>Cán bộ, nhân viên</div>
							</div>
				            				            <div class='sc_home-statics-item'>
								<div class='title'>
									<span class='sc_home-statics-item-number number_transtion' data-content='1000'>1000</span> +								</div>
								<div class='content'>Khách hàng</div>
							</div>
				            				            <div class='sc_home-statics-item'>
								<div class='title'>
									<span class='sc_home-statics-item-number number_transtion' data-content='60'>60</span> +								</div>
								<div class='content'>Tỉnh có mặt SBS</div>
							</div>
				            			</div>
		</div>
	</div>
</div>
<div class='sc_home-about'>
	<div class="container">
		<div class="row">
						            <div class="item col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class='box'>
								<a class='img'><img src='wp-content/uploads/2022/08/cau-chuyen-thuong-hieu.jpg'></a>
								<div class='title' data-animate="animate__rubberBand">Câu chuyện thương hiệu</div>
								<div class='content' data-animate="animate__backInUp">Năm 2018, SBS HOUSE chính thức được thành lập sau hơn 3 năm hoạt động dưới tên gọi là một Team thiết kế M – Design. Sau thành công từ việc đưa cây xanh vào nhà qua  một công trình nhà phố, SBS HOUSE nhen nhuốm hi vọng mang lại những giải pháp kiến trúc tối ưu, nhiều ánh sáng và thông thoáng nhất cho mọi ngôi nhà của người Việt. Từ đó đến nay, chúng tôi vẫn không ngừng theo đuổi mục tiêu này, nỗ lực mỗi ngày để tạo ra “tác phẩm” hơn là sản phẩm cho khách hàng.</div>
								<div class='text-left content-btn mobile_center'>
									<a href='cau-chuyen-thuong-hieu/index.html' class='btnSite btnSite-djf' data-animate="animate__lightSpeedInLeft">
										<span>Xem đầy đủ nội dung</span> 									</a>
								</div>
							</div>
						</div>
			            			            <div class="item col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class='box'>
								<a class='img'><img src='wp-content/uploads/2022/08/bia-02.png'></a>
								<div class='title' data-animate="animate__rubberBand">Giá trị cốt lõi</div>
								<div class='content' data-animate="animate__backInUp">Trung thực – Sáng tạo – Tận tâm – Kỷ luật – Yêu thương là những giá trị cốt lõi mà SBS HOUSE gầy dựng cho đội ngũ. Vì chúng tôi tin rằng, khi sở hữu những giá trị này thì chúng tôi sẽ thành công trong mọi sản phẩm, dịch vụ và tự tin mang đến điều tốt nhất cho khách hàng. Trung thực, tận tâm trong thương thảo, sáng tạo trong mọi sản phẩm, yêu thương và coi công trình của khách hàng như của chính mình. Và kỷ luật để kịp tiến độ dù trong bất cứ hoàn cảnh nào.</div>
								<div class='text-left content-btn mobile_center'>
									<a href='gia-tri-cot-loi/index.html' class='btnSite btnSite-djf' data-animate="animate__lightSpeedInLeft">
										<span>Xem đầy đủ nội dung</span> 									</a>
								</div>
							</div>
						</div>
			            		</div>
	</div>
	
</div>
<div class='sc_home-work'>
	<div class='sc_tab_body'>
		<div class='sc_tab_titles'>
						            <a class='tab_title active' href='#tab_1'>THIẾT KẾ</a>
			            			            <a class='tab_title ' href='#tab_2'>THI CÔNG TRỌN GÓI</a>
			            		</div>
		<div class='sc_home-work-body'>
			<div class="container">
							            <div id='tab_1' class='sc_tab_content active'>
			            	<div class='sc_tab_content_items'>
				            					            		<div class='sc_tab_content_item animation-box1 1animate__delay-1s' data-animate="animate__jackInTheBox">
				            			<div class='sc_tab_top_item'>
					            			<div class='sc_tab_bd'></div>
					            		</div>
					            		<div class='img'>
					            			<img src='wp-content/uploads/2022/07/Artboard-5.png'>
				            			</div>
				            			<div class='title'>TRAO ĐỔI TƯ VẤN</div>
				            			<div class='content'>Trao đổi yêu cầu, tư vấn định hướng ý tường, phong cách và mức đầu tư</div>
				            		</div>
				            						            		<div class='sc_tab_content_item animation-box1 1animate__delay-1s' data-animate="animate__jackInTheBox">
				            			<div class='sc_tab_top_item'>
					            			<div class='sc_tab_bd'></div>
					            		</div>
					            		<div class='img'>
					            			<img src='wp-content/uploads/2022/07/Artboard-6.png'>
				            			</div>
				            			<div class='title'>BÁO GIÁ QUY TRÌNH</div>
				            			<div class='content'>Gửi khách hàng báo giá theo đúng gói thiết kế mà Khách Hàng đang đề cập, kèm quy trình làm việc cụ thể, chi tiết</div>
				            		</div>
				            						            		<div class='sc_tab_content_item animation-box1 1animate__delay-1s' data-animate="animate__jackInTheBox">
				            			<div class='sc_tab_top_item'>
					            			<div class='sc_tab_bd'></div>
					            		</div>
					            		<div class='img'>
					            			<img src='wp-content/uploads/2022/08/Artboard-7.png'>
				            			</div>
				            			<div class='title'>KÝ HỢP ĐỒNG</div>
				            			<div class='content'>Thực hiện các thủ tục hành chính và bắt đầu triển khai các công việc theo tiến độ thống nhất</div>
				            		</div>
				            						            		<div class='sc_tab_content_item animation-box1 1animate__delay-1s' data-animate="animate__jackInTheBox">
				            			<div class='sc_tab_top_item'>
					            			<div class='sc_tab_bd'></div>
					            		</div>
					            		<div class='img'>
					            			<img src='wp-content/uploads/2022/07/Artboard-8.png'>
				            			</div>
				            			<div class='title'>BÀN GIAO & QUYẾT TOÁN</div>
				            			<div class='content'>Sau khi thống nhất hồ sơ báo cáo tiến độ, khách hàng thanh toán lần cuối giá trị HĐ còn lại trước khi nhận hồ sơ hoàn chỉnh.</div>
				            		</div>
				            						            </div>
			            </div>
			            			            <div id='tab_2' class='sc_tab_content '>
			            	<div class='sc_tab_content_items'>
				            					            		<div class='sc_tab_content_item animation-box1 1animate__delay-1s' data-animate="animate__jackInTheBox">
				            			<div class='sc_tab_top_item'>
					            			<div class='sc_tab_bd'></div>
					            		</div>
					            		<div class='img'>
					            			<img src='wp-content/uploads/2022/07/Artboard-5.png'>
				            			</div>
				            			<div class='title'>TRAO ĐỔI TƯ VẤN</div>
				            			<div class='content'>Trao đổi và tư vấn khách hàng về nhu cầu, mong muốn, và định hướng mức đầu tư.</div>
				            		</div>
				            						            		<div class='sc_tab_content_item animation-box1 1animate__delay-1s' data-animate="animate__jackInTheBox">
				            			<div class='sc_tab_top_item'>
					            			<div class='sc_tab_bd'></div>
					            		</div>
					            		<div class='img'>
					            			<img src='wp-content/uploads/2022/07/Artboard-6.png'>
				            			</div>
				            			<div class='title'>BÁO GIÁ</div>
				            			<div class='content'>Gửi báo giá thi công, chủng loại vật tư và Quy trình thi công để khách hàng nắm được thông tin.</div>
				            		</div>
				            						            		<div class='sc_tab_content_item animation-box1 1animate__delay-1s' data-animate="animate__jackInTheBox">
				            			<div class='sc_tab_top_item'>
					            			<div class='sc_tab_bd'></div>
					            		</div>
					            		<div class='img'>
					            			<img src='wp-content/uploads/2022/08/Artboard-9.png'>
				            			</div>
				            			<div class='title'>KÝ HỢP ĐỒNG</div>
				            			<div class='content'>Hai bên gặp gỡ trao đổi thống nhất các vấn đề liên quan tiến độ, chất lượng, ngày khởi công và các điều khoản hợp đồng.</div>
				            		</div>
				            						            		<div class='sc_tab_content_item animation-box1 1animate__delay-1s' data-animate="animate__jackInTheBox">
				            			<div class='sc_tab_top_item'>
					            			<div class='sc_tab_bd'></div>
					            		</div>
					            		<div class='img'>
					            			<img src='wp-content/uploads/2022/08/Artboard-10.png'>
				            			</div>
				            			<div class='title'>BÀN GIAO & QUYẾT TOÁN</div>
				            			<div class='content'>Kiểm tra, nghiệm thu và thanh quyết toán hợp đồng. Tiến hành bảo hành bảo trì dài hạn theo cam kết hợp đồng.</div>
				            		</div>
				            						            </div>
			            </div>
			            			</div>
		</div>
	</div>
</div>
<div class='sc_home-project'>
		            <div class="container-fluid pd-0 ">
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 keep_left">
						<div class='sc_home-project-top'>
							<h2 class='sc_home-project-title' data-animate="animate__bounceIn">
								Công trình thiết kế							</h2>
							<div class='content_p sc_home-project-content'><p>Mỗi năm, SBS HOUSE thực hiện hàng trăm công trình thiết kế ở mọi miền đất nước. Phong cách thiết kế chính của SBS HOUSE là hiện đại - tối giản - tiện nghi - thông thoáng. Ngoài ra, những ý tưởng và sở thích của gia chủ cũng được ưu tiên hàng đầu, để tạo nên một công trình nhà ở độc bản, mang đậm dấu ấn cá nhân.</p>
</div>
							<div class='mg-t-30 mobile_center'>
								<a href='chuyen-muc/mau-nha-dep/cong-trinh-thiet-ke/index.html' class='btnSite btnSite-djf'>
									<span>Xem tất cả</span> 								</a>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 keep_right">
																			<div class='sc_home-project-sld-1'>
								<div class='sc_home-slide_project sc_slide' items="3" items-tablet="2" items-mobile="1" margin="30" nav="<img src='wp-content/themes/sbshouse/images/icon_slide-left.png'>|<img src='wp-content/themes/sbshouse/images/icon_slide-right.png'>">
															    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/06/COVER-WEB-SBS-1-1.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='ts-house-gia-tri-cua-nhung-duong-cong-quyen-ru/index.html'>T’s House &#8211; Giá trị của những đường cong quyến rũ </a>
						    							<div class='fs-13 mg-t-5'>
						    								Trong hành trình dài rộng đầy biến đổi của cuộc đời, mỗi người chúng ta đều mong muốn tìm kiếm một bến đỗ bình yên, nơi an trú cho cả...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='ts-house-gia-tri-cua-nhung-duong-cong-quyen-ru/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/07/biet-phu-sang-trong-34.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='biet-thu-tan-co-dien-noi-bat/index.html'>Biệt thự Tân cổ điển nổi bật với các thức cột đối xứng tinh tế</a>
						    							<div class='fs-13 mg-t-5'>
						    								B’s Villa nổi bật với vẻ đẹp duyên dáng và tinh tế với những thức cột đối xứng nhau ấn tượng. Một phong cách thường thấy của các quốc gia...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='biet-thu-tan-co-dien-noi-bat/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/07/20230206-ngt-2-scaled.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='biet-thu-phong-cach-modern-luxury/index.html'>S&#8217;s Villa &#8211; Modern Luxury phong cách thời thượng nâng tầm đẳng cấp</a>
						    							<div class='fs-13 mg-t-5'>
						    								Tưởng chừng là phong cách hiện đại thuần túy với đường nét mạnh mẽ, phóng khoáng. S's Villa lại có thiết kế nội thất theo phong cách thời thượng mang...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='biet-thu-phong-cach-modern-luxury/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/05/ng-t-2-2-scaled.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='ds-house-ban-tinh-ca-cua-anh-sang-va-phong-cach-hien-dai/index.html'>Đ’s House &#8211; Bản tình ca của ánh sáng và phong cách hiện đại</a>
						    							<div class='fs-13 mg-t-5'>
						    								Một thiết kế theo lối kiến trúc hiện đại, tinh tế hòa cùng sắc màu tươi tắn của ánh sáng như một bản tình ca dịu êm xoa dịu tâm...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='ds-house-ban-tinh-ca-cua-anh-sang-va-phong-cach-hien-dai/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/05/COVER-WEB-SBS-3.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='ts-house-nha-pho-3-tang-voi-kien-truc-hien-dai-phong-thoang/index.html'>T’s House &#8211; Nhà phố 3 tầng tinh tế, phóng khoáng cùng tháng năm</a>
						    							<div class='fs-13 mg-t-5'>
						    								T’s House là một dự án thiết kế mà SBS HOUSE đã thực hiện cho một gia chủ ở Biên Hòa. Với phong cách hiện đại và sang trọng, T’s...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='ts-house-nha-pho-3-tang-voi-kien-truc-hien-dai-phong-thoang/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/03/Can-ho-hien-dai-4.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='can-ho-tien-nghi-khong-thua-kem-nha-o/index.html'>Căn hộ tiện nghi không thua kém nhà ở</a>
						    							<div class='fs-13 mg-t-5'>
						    								Vài năm gần đây, nhiều chủ đầu tư tập trung vào hình thức kinh doanh căn hộ vì đây là một trong những xu hướng sinh sống của nhiều người...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='can-ho-tien-nghi-khong-thua-kem-nha-o/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/03/COVER-WEB-SBS-1.png'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='nha-o-hien-dai/index.html'>D’s House &#8211; Thổi Hồn Vào Thiết Kế Nhà Ở Hiện Đại</a>
						    							<div class='fs-13 mg-t-5'>
						    								Phong cách thiết kế nhà ở hiện đại đã không còn quá xa lạ với những người trẻ năng động, yêu thích sự tiện nghi. D’s House được ra đời...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='nha-o-hien-dai/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/01/11-1-scaled.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='dam-say-biet-thu-hs-villa-tai-da-nang-noi-that-phong-cach-dia-trung-hai/index.html'>Đắm say biệt thự H’s Villa tại Đà Nẵng &#8211; Nội thất phong cách Địa trung hải</a>
						    							<div class='fs-13 mg-t-5'>
						    								Chiêm ngưỡng không gian kiến trúc, nội thất mới lạ và cảnh quan sân vườn biệt thự tuyệt đẹp, đậm màu sắc hiện đại với không gian mở thoáng đãng...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='dam-say-biet-thu-hs-villa-tai-da-nang-noi-that-phong-cach-dia-trung-hai/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2022/09/7.7.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='nhans-villa-tan-huong-cuoc-song-tu-chon-nghi-duong-cang-tran-hoi-tho-luxury/index.html'>Nhan’s Villa &#8211; tận hưởng cuộc sống từ “chốn nghỉ dưỡng” căng tràn hơi thở luxury</a>
						    							<div class='fs-13 mg-t-5'>
						    								Bạn định nghĩa như thế nào về một ngôi nhà hoàn hảo? Với chúng tôi có lẽ là tổ ấm mà nơi đó các thành viên có sự gắn kết...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='nhans-villa-tan-huong-cuoc-song-tu-chon-nghi-duong-cang-tran-hoi-tho-luxury/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    										</div>
							</div>
														<div class='sc_home-project-sld-2'>
								<div class='sc_home-slide_project sc_slide' items="3" items-tablet="2" items-mobile="1" margin="30" nav="<img src='wp-content/themes/sbshouse/images/icon_slide-left.png'>|<img src='wp-content/themes/sbshouse/images/icon_slide-right.png'>">
															    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2021/05/bi%e1%bb%87t-th%e1%bb%b1-%c4%91%e1%ba%b9p-hi%e1%bb%87n-%c4%91%e1%ba%a1i-3.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='biet-thu-sang-trong/index.html'>Biệt thự 3 tầng sang trọng &#8211; Tổng kinh phí đầu tư 9 Tỷ</a>
						    							<div class='fs-13 mg-t-5'>
						    								Ngôi biệt thự 3 tầng được xây dựng trên khu đất 14x22,5m và tổng diện tích sử dụng lên đến 456m2. Đây là một công trình mang một phong cách...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='biet-thu-sang-trong/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2022/04/3-1.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='ks-house-khong-gian-mo-hien-dai-chat-lu-cho-gia-chu-quang-ngai/index.html'>K’s House &#8211; Nhà 2 tầng không gian mở hiện đại “chất lừ” cho gia chủ Quảng Ngãi</a>
						    							<div class='fs-13 mg-t-5'>
						    								Hơn cả một không gian sống đơn thuần, một mái ấm bền vững còn là nơi tiếp thêm nguồn năng lượng tích cực và kết nối các thành viên trong...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='ks-house-khong-gian-mo-hien-dai-chat-lu-cho-gia-chu-quang-ngai/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2022/04/nha-1-tang-sang-trong-5.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='man-nhan-voi-thiet-ke-hien-dai/index.html'>Mãn nhãn với thiết kế hiện đại, phong cách luxury và tone màu tối chủ đạo</a>
						    							<div class='fs-13 mg-t-5'>
						    								Một căn nhà không cần quá lớn nhưng đủ ấm áp để mỗi thành viên đều mong trở về, đủ sang trọng để tiếp đón những vị khách, đủ tiện...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='man-nhan-voi-thiet-ke-hien-dai/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2022/03/biet-thu-2-tang-soc-trang-1.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='kien-truc-hien-dai-2-tang/index.html'>Ngẩn ngơ trước không gian xanh mát trong kiến trúc hiện đại 2 tầng</a>
						    							<div class='fs-13 mg-t-5'>
						    								Nhà không chỉ là không gian sống mà còn là tổ ấm hạnh phúc của mỗi gia đình. Chính vì vậy, việc tạo nên một ngôi nhà đẹp là vô...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='kien-truc-hien-dai-2-tang/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2022/02/Nha-3-tang-dep-ngat-ngay-2.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='sieu-pham-nha-hien-dai-3-tang/index.html'>P&#8217;s Villa &#8211; siêu phẩm nhà hiện đại 3 tầng không kém khu nghỉ dưỡng cao cấp</a>
						    							<div class='fs-13 mg-t-5'>
						    								Sự hài lòng của quý khách hàng chính là động lực to lớn để SBS HOUSE làm tốt hơn nữa sứ mệnh dựng xây “tổ ấm” hạnh phúc. Như một...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='sieu-pham-nha-hien-dai-3-tang/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2021/06/bi%e1%bb%87t-th%e1%bb%b1-2-t%e1%ba%a7ng-%c4%91%e1%ba%b9p-1.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='biet-thu-2-tang-mai-thai-hien-dai/index.html'>Biệt thự 2 tầng mái thái hiện đại</a>
						    							<div class='fs-13 mg-t-5'>
						    								Để tiếp nối chuỗi series biệt thự 2 tầng. Hôm nay, SBS HOUSE xin giới thiệu đến mọi người một căn biệt thự 2 tầng cực kì đẹp. Với phong...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='biet-thu-2-tang-mai-thai-hien-dai/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2022/03/Thiet-ke-chua-co-ten.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='hien-dai-va-tho-moc-tinh-te-va-sang-trong-hoi-tu-trong-can-nha-3-tang-1-tum-tai-da-nang/index.html'>Hiện đại và thô mộc, tinh tế và sang trọng hội tụ trong căn nhà 3 tầng 1 tum tại Đà Nẵng</a>
						    							<div class='fs-13 mg-t-5'>
						    								D’s House - Ngôi nhà đặt tên là "yêu thương" được SBS HOUSE thiết kế dành cho gia đình tại Đà Nẵng. Chủ nhà là tuýp người đề cao sự...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='hien-dai-va-tho-moc-tinh-te-va-sang-trong-hoi-tu-trong-can-nha-3-tang-1-tum-tai-da-nang/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2022/03/MT1.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='hs-house-su-ket-hop-tuyet-voi-cua-phong-cach-nhiet-doi-va-phong-cach-tho-moc/index.html'>H’s House &#8211; sự kết hợp tuyệt vời của phong cách Nhiệt đới và phong cách Thô mộc</a>
						    							<div class='fs-13 mg-t-5'>
						    								Như chúng ta đã biết những năm gần đây, xu hướng thiết kế nhà ở theo phong cách hiện đại đang “lên ngôi”. Vì nhà ở hiện đại đáp ứng...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='hs-house-su-ket-hop-tuyet-voi-cua-phong-cach-nhiet-doi-va-phong-cach-tho-moc/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2022/03/MT2.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='nha-pho-hien-dai-3-tang-nhieu-cay-xanh/index.html'>Nhà phố hiện đại 3 tầng nhiều cây xanh, mặt tiền 5m ấn tượng</a>
						    							<div class='fs-13 mg-t-5'>
						    								Những mẫu nhà phố 3 tầng hiện nay quả thực đã chiếm trọn trái tim của nhiều gia đình Việt. Với rất nhiều phong cách khác nhau, nhà 3 tầng...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='nha-pho-hien-dai-3-tang-nhieu-cay-xanh/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2021/12/Thi%e1%ba%bft-k%e1%ba%bf-nh%c3%a0-3-t%e1%ba%a7ng-1-tum-%c4%91%e1%ba%b9p-1.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='hs-house-nha-3-tang-hien-dai-dan-xen-tho-moc/index.html'>H’S HOUSE &#8211; NHÀ 3 TẦNG HIỆN ĐẠI TẠI ĐÀ NẴNG</a>
						    							<div class='fs-13 mg-t-5'>
						    								Cùng là  mẫu nhà ống đẹp 3 tầng 5m như nhiều ngôi nhà phố khác, nhưng H’s House nổi bật nhờ thiết kế ấn tượng. Có thể nói, đây là...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='hs-house-nha-3-tang-hien-dai-dan-xen-tho-moc/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2021/12/nh%c3%a0-%c4%91%e1%ba%b9p-2-t%e1%ba%a7ng-1.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='nha-hai-tang-xinh-xan-voi-nhieu-dac-diem-kien-truc-an-tuong/index.html'>NHÀ 2 TẦNG XINH XẮN VỚI NHIỀU ĐẶC ĐIỂM KIẾN TRÚC ẤN TƯỢNG</a>
						    							<div class='fs-13 mg-t-5'>
						    								Nếu bạn yêu thích phong cách nhà ở hiện đại kết hợp cùng những chi tiết kiến trúc đơn giản mà ấn tượng thì Ha’s House là mẫu nhà 2...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='nha-hai-tang-xinh-xan-voi-nhieu-dac-diem-kien-truc-an-tuong/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    										</div>
							</div>
												</div>
				</div>
	            	            <div class="container-fluid pd-0 sc_home-project-els">
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 keep_left">
						<div class='sc_home-project-top'>
							<h2 class='sc_home-project-title' data-animate="animate__bounceIn">
								Công trình thi công							</h2>
							<div class='content_p sc_home-project-content'><p>Tất cả công trình thi công do SBS HOUSE thực hiện đều đảm bảo những giải pháp mới và tối ưu nhất nhằm mang đến một sản phẩm kiên cố, bền vững. Mặc dù thi công nhà phố nhưng từ hạng mục lớn nhỏ đều được áp dụng kỹ thuật thi công nhà cao tầng Coteccons. Hơn thế nữa, các công trình được thiết kế - thi công trọn gói sẽ giống với bản vẽ thiết kế ít nhất 95%.</p>
</div>
							<div class='mg-t-30 mobile_center'>
								<a href='chuyen-muc/cong-trinh-thi-cong/index.html' class='btnSite btnSite-djf'>
									<span>Xem tất cả</span> 								</a>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 keep_right">
																			<div class='sc_home-project-sld-1'>
								<div class='sc_home-slide_project sc_slide' items="3" items-tablet="2" items-mobile="1" margin="30" nav="<img src='wp-content/themes/sbshouse/images/icon_slide-left.png'>|<img src='wp-content/themes/sbshouse/images/icon_slide-right.png'>">
															    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/07/nha-dep-thuc-te-31-of-31.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='nha-3-tang-hien-dai-5x26m/index.html'>Hình ảnh nhà phố thực tế C’s House tại Hà Tĩnh</a>
						    							<div class='fs-13 mg-t-5'>
						    								SBS HOUSE hiểu rằng nhà không chỉ là ngoại thất đẹp mắt, mà còn là không gian sống tiện nghi và bền vững. C's House là ngôi nhà 3 tầng hiện...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='nha-3-tang-hien-dai-5x26m/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/06/Ngoi-nha-xanh-24.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='nha-thuc-te-3-tang-5x20m/index.html'>Công trình thực tế: Không gian lý tưởng kết hợp tự nhiên và hiện đại</a>
						    							<div class='fs-13 mg-t-5'>
						    								Chuyến hành trình ghé thăm những công trình thi công, lần này SBS HOUSE dừng chân tại T's House - ngôi nhà 3 tầng hiện đại tọa lạc tại thành...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='nha-thuc-te-3-tang-5x20m/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/05/Nha-thuc-te-3-tang-33.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='nha-pho-3-tang-5m/index.html'>Tham quan thực tế nhà phố 3 tầng 5m sau 2 năm sử dụng</a>
						    							<div class='fs-13 mg-t-5'>
						    								Theo thời gian, mỗi chuẩn mực đều thay đổi và biến tấu theo cách riêng, nhưng điều cuối cùng ở lại vẫn là “cái đẹp” của cảm xúc… C’s House...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='nha-pho-3-tang-5m/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/08/nha-dep-3-tang-thuc-te-4.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='ds-house-nha-3-tang-dien-tich-dat-8x23m/index.html'>Công trình thực tế D&#8217;s House &#8211; Khám phá một cuộc sống không giới hạn</a>
						    							<div class='fs-13 mg-t-5'>
						    								D's House nằm trên mảnh đất rộng 181.7m2 với diện tích xây dựng 269m2, được thiết kế theo phong cách hiện đại độc đáo. Ngôi nhà không chỉ là một...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='ds-house-nha-3-tang-dien-tich-dat-8x23m/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/03/Nha-cap-4-don-gian-dep-3-scaled.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='tham-quan-nha-cap-4-thuc-te-tai-quang-binh-ts-house/index.html'>Tham quan nhà cấp 4 thực tế tại Quảng Bình &#8211; T’s House</a>
						    							<div class='fs-13 mg-t-5'>
						    								Giữa những công trình nhà phố cao tầng, T’s House trở thành nốt lặng cho những an yên.. Vừa rồi SBS HOUSE có dịp ghé thăm tổ ấm của anh...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='tham-quan-nha-cap-4-thuc-te-tai-quang-binh-ts-house/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/03/0-scaled.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='ds-house-nha-o-hien-dai/index.html'>Đ’s House &#8211; Cho ngày nắng lên mang ánh sáng vào nhà</a>
						    							<div class='fs-13 mg-t-5'>
						    								Phong cách hiện đại, tối giản đang là xu hướng ưa chuộng trong những năm gần đây. Một ngôi nhà chú trọng nội thất và công năng sẽ tạo nên...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='ds-house-nha-o-hien-dai/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/03/COVER-WEB-SBS-1-2.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='hs-house-hien-dai/index.html'>H’s House &#8211; Bản hòa ca giữa Hiện đại và Tân cổ điển &#8211; Công trình thực tế</a>
						    							<div class='fs-13 mg-t-5'>
						    								Ngôi nhà sang trọng H’s house được thiết kế theo lối kiến trúc hiện đại pha lẫn yếu tố tân cổ điển. Lược bỏ đi những họa tiết cầu kỳ,...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='hs-house-hien-dai/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/03/mat-tien-biet-thu-2-tang.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='ps-house-cong-trinh-2-tang-thuc-te-hien-dai-va-thong-thoang/index.html'>P’s House &#8211; Công trình 2 tầng thực tế hiện đại và thông thoáng</a>
						    							<div class='fs-13 mg-t-5'>
						    								Một siêu phẩm nhà phố 2 tầng hiện đại tiếp theo đã được SBS HOUSE hoàn thiện và gửi về cho gia chủ tại Đà Nẵng. Không gian thoáng mát...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='ps-house-cong-trinh-2-tang-thuc-te-hien-dai-va-thong-thoang/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/02/nha-linh-van-tien-dung.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='nha-la-noi-khoi-nguon-hanh-phuc/index.html'>Nhà là nơi khởi nguồn hạnh phúc</a>
						    							<div class='fs-13 mg-t-5'>
						    								L’s House là một trong những công trình thực tế được SBS HOUSE hoàn thiện vào đầu năm 2023. Căn nhà có 4 tầng 1 tum, kết hợp 2 hệ...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='nha-la-noi-khoi-nguon-hanh-phuc/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    										</div>
							</div>
														<div class='sc_home-project-sld-2'>
								<div class='sc_home-slide_project sc_slide' items="3" items-tablet="2" items-mobile="1" margin="30" nav="<img src='wp-content/themes/sbshouse/images/icon_slide-left.png'>|<img src='wp-content/themes/sbshouse/images/icon_slide-right.png'>">
															    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/02/Nha-3-tang-thuc-te-3.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='hoan-hao-tu-thiet-ke-den-thuc-te-voi-nha-3-tang-tys-house/index.html'>Hoàn hảo từ thiết kế đến thực tế với nhà 3 tầng TY’s House</a>
						    							<div class='fs-13 mg-t-5'>
						    								Với mong muốn mang đến cho khách hàng không gian sống hoàn mỹ nhất, SBS HOUSE không chỉ dừng lại ở việc thiết kế mà còn hỗ trợ khách hàng...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='hoan-hao-tu-thiet-ke-den-thuc-te-voi-nha-3-tang-tys-house/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/02/1-6.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='khong-gian-song-thanh-binh-trong-biet-thu-2-tang-1-tum-tai-quang-ngai/index.html'>D’s Villa &#8211; Không gian sống thanh bình trong biệt thự 2 tầng 1 tum tại Quảng Ngãi</a>
						    							<div class='fs-13 mg-t-5'>
						    								SBS HOUSE luôn ưu tiên mang đến những công trình hiện đại, những không gian có sự kết nối gia đình và thiên nhiên trong tổ ấm. Vừa qua, SBS...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='khong-gian-song-thanh-binh-trong-biet-thu-2-tang-1-tum-tai-quang-ngai/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2023/02/Nha-3-tang-mat-tien-doc-dao-73.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='ms-house-3-tang-tu-3d-ra-thuc-te/index.html'>M’s House 3 tầng &#8211; Từ 3D ra thực tế</a>
						    							<div class='fs-13 mg-t-5'>
						    								Căn nhà 3 tầng này được SBS HOUSE thiết kế cho chị M tại Đà Nẵng năm 2022, tới nay đã xuất hiện ngoài thực tế với “hình hài” hoàn...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='ms-house-3-tang-tu-3d-ra-thuc-te/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2022/12/Nha-2-tang-hien-dai-nhe-nhang-2-scaled.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='cong-trinh-thuc-te-2-tang-cs-house-to-am-nhe-nhang-an-yen/index.html'>Công trình thực tế 2 tầng C’s House &#8211; tổ ấm nhẹ nhàng, an yên </a>
						    							<div class='fs-13 mg-t-5'>
						    								Cuối năm luôn là dịp SBS HOUSE đón chào sự hoàn thiện của rất nhiều công trình. Một trong số đó có ngôi nhà 2 tầng do chúng tôi thiết...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='cong-trinh-thuc-te-2-tang-cs-house-to-am-nhe-nhang-an-yen/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2022/12/Nha-2-tang-1-tum-doc-la-58.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='chao-don-cong-trinh-thuc-te-hien-dai-voi-mat-tien-doc-dao-tai-da-nang/index.html'>Chào đón công trình thực tế hiện đại với mặt tiền độc đáo tại Đà Nẵng</a>
						    							<div class='fs-13 mg-t-5'>
						    								H's House là công trình mà đội ngũ SBS HOUSE may mắn được đồng hành cùng gia chủ trong cả quá trình thiết kế và thi công trọn gói. Ngày...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='chao-don-cong-trinh-thuc-te-hien-dai-voi-mat-tien-doc-dao-tai-da-nang/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2022/12/Nha-5-tang-2-mat-tien-5-scaled.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='mau-nha-2-mat-tien-ket-hop-kinh-doanh-cong-trinh-thuc-te-tai-da-nang/index.html'>Mẫu nhà 2 mặt tiền kết hợp kinh doanh &#8211; Công trình thực tế tại Đà Nẵng</a>
						    							<div class='fs-13 mg-t-5'>
						    								SBS HOUSE tự hào khi tiếp tục đón thêm một “thành viên” mới trong đại gia đình SBS - công trình nhà phố 5 tầng hiện đại T’s House. Sản...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='mau-nha-2-mat-tien-ket-hop-kinh-doanh-cong-trinh-thuc-te-tai-da-nang/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2022/12/4-11.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='nha-vuon-mai-nhat-tai-quang-tri-noi-luu-giu-hanh-phuc/index.html'>Nhà vườn mái nhật tại Quảng Trị &#8211; Nơi lưu giữ hạnh phúc </a>
						    							<div class='fs-13 mg-t-5'>
						    								Mỗi công trình là một câu chuyện và kỷ niệm riêng của gia chủ, kiến trúc sư và cả những người xây dựng. Hơn thế nữa, còn là niềm tự...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='nha-vuon-mai-nhat-tai-quang-tri-noi-luu-giu-hanh-phuc/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2022/10/biet-thu-3-tang-hien-dai-tai-tam-ky-71.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='hoan-thien-tron-goi-ls-house-nha-pho-2-tang-1-tum-hien-dai-tai-quang-nam/index.html'>Hoàn thiện trọn gói L’s House &#8211; nhà phố 2 tầng 1 tum hiện đại tại Quảng Nam</a>
						    							<div class='fs-13 mg-t-5'>
						    								Đối với SBS HOUSE mỗi công trình đi qua đều là những dấu ấn riêng biệt, về công năng, thẩm mỹ, khuôn đất cũng như thời thế ở hiện tại,...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='hoan-thien-tron-goi-ls-house-nha-pho-2-tang-1-tum-hien-dai-tai-quang-nam/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2022/09/Mat-tien-1.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='nha-pho-mang-net-kien-truc-ban-dia-doc-dao-tai-da-nang/index.html'>Nhà phố mang nét kiến trúc bản địa độc đáo tại Đà Nẵng</a>
						    							<div class='fs-13 mg-t-5'>
						    								72MCT House là một ngôi nhà lô phố mang hơi hướng kiến trúc bản địa, thể hiện cụ thể qua hệ mái được thiết kế theo lối kiến trúc Hội...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='nha-pho-mang-net-kien-truc-ban-dia-doc-dao-tai-da-nang/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2022/09/1-4-1.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='khong-gian-lam-viec-sang-trong-chuan-thuong-hieu-khang-dinh-dang-cap-vuot-troi/index.html'> Không gian làm việc sang trọng, chuẩn thương hiệu khẳng định đẳng cấp vượt trội </a>
						    							<div class='fs-13 mg-t-5'>
						    								Không gian làm việc đóng một vai trò rất quan trọng cho sự thành công của mỗi doanh nghiệp. SBS HOUSE rất vinh dự khi là đơn vị trúng thầu...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='khong-gian-lam-viec-sang-trong-chuan-thuong-hieu-khang-dinh-dang-cap-vuot-troi/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    								    		<div class="item">
						    			<div class='sc_home-project-item bg-animation-box' data-animate="animate__zoomIn">
						    				<img src='wp-content/uploads/2022/08/0.jpg'>
						    				<div class='content'>
						    					<div class='title'>
						    						<div class='text-left'>
						    							<a href='cong-trinh-thuc-te-nha-hs-house-2-tang-hien-dai-va-thoang-mat/index.html'>Công trình thực tế nhà H’s House 2 tầng hiện đại và thoáng mát</a>
						    							<div class='fs-13 mg-t-5'>
						    								Bên cạnh thiết kế, SBS HOUSE còn cung cấp dịch vụ thi công trọn gói cho khách hàng. Hầu hết các công trình thi công của chúng tôi đều được...						    							</div>
						    						</div>
						    					</div>
						    					<div class='refLink'>
						    						<a href='cong-trinh-thuc-te-nha-hs-house-2-tang-hien-dai-va-thoang-mat/index.html'>
							    						<span>Xem thêm</span> 							    					</a>
							    				</div>
						    				</div>
						    			</div>
						    		</div>
						    										</div>
							</div>
												</div>
				</div>
	            </div>
<div class="container">
	<div class='sc_home-gallery'>
		<h2 class='text-center fw-600 fs-40 mg-b-30'>
			<span class='hLine hLineS hLineSs'>Hình ảnh hoạt động</span>
		</h2>
					<div class='sc_home-gallery-items gallery-event lightGallery-event'>
									<a class='item' data-src='wp-content/uploads/2022/08/anh-hoat-dong-sbshouse-30.jpg'>
				    	<img src="wp-content/uploads/2022/08/anh-hoat-dong-sbshouse-30.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2022/08/anh-hoat-dong-sbs-4.jpg'>
				    	<img src="wp-content/uploads/2022/08/anh-hoat-dong-sbs-4.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2023/04/chay-e1681208246543.jpg'>
				    	<img src="wp-content/uploads/2023/04/chay-e1681208246543.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2023/04/SBSRunning-1-e1681208310196.jpg'>
				    	<img src="wp-content/uploads/2023/04/SBSRunning-1-e1681208310196.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2023/04/running-e1681208161701.jpg'>
				    	<img src="wp-content/uploads/2023/04/running-e1681208161701.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2022/08/anh-hoat-dong-sbs-5.jpg'>
				    	<img src="wp-content/uploads/2022/08/anh-hoat-dong-sbs-5.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2022/08/anh-hoat-dong-sbs-13.jpg'>
				    	<img src="wp-content/uploads/2022/08/anh-hoat-dong-sbs-13.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2022/08/anh-hoat-dong-sbs-15.jpg'>
				    	<img src="wp-content/uploads/2022/08/anh-hoat-dong-sbs-15.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2022/08/anh-hoat-dong-sbs-6.jpg'>
				    	<img src="wp-content/uploads/2022/08/anh-hoat-dong-sbs-6.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2022/08/anh-hoat-dong-sbs-8-e1685346893855.jpg'>
				    	<img src="wp-content/uploads/2022/08/anh-hoat-dong-sbs-8-e1685346893855.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2022/08/anh-hoat-dong-sbs-1.jpg'>
				    	<img src="wp-content/uploads/2022/08/anh-hoat-dong-sbs-1.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2022/08/anh-hoat-dong-sbs-12.jpg'>
				    	<img src="wp-content/uploads/2022/08/anh-hoat-dong-sbs-12.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2022/08/anh-hoat-dong-sbs-10.jpg'>
				    	<img src="wp-content/uploads/2022/08/anh-hoat-dong-sbs-10.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2022/08/anh-hoat-dong-sbs-9.jpg'>
				    	<img src="wp-content/uploads/2022/08/anh-hoat-dong-sbs-9.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2022/08/anh-hoat-dong-sbs-17.jpg'>
				    	<img src="wp-content/uploads/2022/08/anh-hoat-dong-sbs-17.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2022/08/anh-hoat-dong-sbs-23.jpg'>
				    	<img src="wp-content/uploads/2022/08/anh-hoat-dong-sbs-23.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2023/04/noel-e1681208095479.jpg'>
				    	<img src="wp-content/uploads/2023/04/noel-e1681208095479.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2023/04/daotao1-e1685151844208.jpg'>
				    	<img src="wp-content/uploads/2023/04/daotao1-e1685151844208.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2023/04/TTD_6686-e1681208189102.jpg'>
				    	<img src="wp-content/uploads/2023/04/TTD_6686-e1681208189102.jpg">
				    </a>
										<a class='item' data-src='wp-content/uploads/2023/04/TTD_6146-e1681208208755.jpg'>
				    	<img src="wp-content/uploads/2023/04/TTD_6146-e1681208208755.jpg">
				    </a>
								</div>
				</div>
</div>
<div class='sc_home-bst'>
	<div class='title'>SIMPLE<br>BUT SIGNIFICANT</div>
	<div class='sc_home-bst-box'>
		<div class='sc_home-bst-item'>
			<div class='io2 io2-doubleMobile'>
				<div class='cl io21'>Điểm khác biệt<br>thi công của SBS</div>
				<div class='io22'>Để có những công trình chất lượng, chúng tôi luôn cải tiến, đổi mới từng ngày. Trong cùng một phân khúc nhà phố, SBS HOUSE tự hào vì có những điểm khác biệt trong giải pháp, kỹ thuật thi công. Chính những điểm khác biệt nhỏ này tạo nên một khác biệt rất lớn, từ đó cho ra đời những công trình chuẩn chỉnh, bền vững với thời gian.</div>
				<div class=''>
					<a class='btnSite btnSite-djf' href='diem-khac-biet-trong-thi-cong-cua-sbs-house/index.html'>
						<span>XEM THÊM</span> 					</a>
				</div>
			</div>
			<div class='io1 io1-full'>
				<div class='sc_slide hasdot mg-t-15 sc_slide_single'>
											<img src='wp-content/uploads/2022/08/Diem-khac-biet-01.jpg'>
												<img src='wp-content/uploads/2022/08/Diem-khac-biet-02.jpg'>
												<img src='wp-content/uploads/2022/08/Diem-khac-biet-03.jpg'>
												<img src='wp-content/uploads/2022/08/Diem-khac-biet-04.jpg'>
												<img src='wp-content/uploads/2022/08/Diem-khac-biet-05.jpg'>
										</div>
			</div>
			<div class='io2 io2-doubleMobile1'>
				<div class='cl io21'>Điểm khác biệt<br>thi công của SBS</div>
				<div class='io22'>Để có những công trình chất lượng, chúng tôi luôn cải tiến, đổi mới từng ngày. Trong cùng một phân khúc nhà phố, SBS HOUSE tự hào vì có những điểm khác biệt trong giải pháp, kỹ thuật thi công. Chính những điểm khác biệt nhỏ này tạo nên một khác biệt rất lớn, từ đó cho ra đời những công trình chuẩn chỉnh, bền vững với thời gian.</div>
				<div class=''>
					<a class='btnSite btnSite-djf' href='diem-khac-biet-trong-thi-cong-cua-sbs-house/index.html'>
						<span>XEM THÊM</span> 					</a>
				</div>
			</div>
		</div>
	</div>
	<div class='sc_home-bst-box'>
		<div class='sc_home-bst-item'>
			<div class='io2'>
				<div class='cl io21'>Lễ động thổ<br>Lễ ký hợp đồng</div>
				<div class='io22'>Lễ động thổ, lễ ký hợp đồng luôn được chuẩn bị chu đáo và trang trọng để quý khách hàng hiểu rằng SBS HOUSE trân trọng sự hợp tác cùng quý khách.
Chúng tôi không thể mang đến cho quý khách giá thành rẻ, chúng tôi chỉ có thể mang đến sản phẩm tốt và dịch vụ chất lượng hàng đầu.</div>
				<div class=''>
					<a class='btnSite btnSite-djf' href='le-dong-tho/index.html'>
						<span>XEM THÊM</span> 					</a>
				</div>
			</div>
			<div class='io1 io1-full'>
				<div class='sc_slide hasdot'>
											<img src='wp-content/uploads/2023/04/Ky-HD-nha-Dang-Bao-Hung-01.jpg'>
												<img src='wp-content/uploads/2023/04/Ky-HD-nha-Loc-01.jpg'>
												<img src='wp-content/uploads/2023/04/Ky-HD-Nguyen-Lam-Minh-Thai-01.jpg'>
												<img src='wp-content/uploads/2023/04/Ky-HD-Lo-Van-Truong-01.jpg'>
												<img src='wp-content/uploads/2023/04/Ky-HD-nha-a-Thanh-01.jpg'>
												<img src='wp-content/uploads/2023/04/le-dong-tho-nha-Tuan-01-min.jpg'>
												<img src='wp-content/uploads/2023/04/le-dong-tho-Tran-Van-Hieu-01-min.jpg'>
												<img src='wp-content/uploads/2023/04/le-dong-tho-Mr.-Ho-01-min.jpg'>
												<img src='wp-content/uploads/2022/10/mo-mong-03.jpg'>
												<img src='wp-content/uploads/2022/10/2-03.jpg'>
												<img src='wp-content/uploads/2022/08/dong-tho-2.jpg'>
												<img src='wp-content/uploads/2022/08/dong-tho-4.jpg'>
												<img src='wp-content/uploads/2022/08/dong-tho-5.jpg'>
												<img src='wp-content/uploads/2022/08/dong-tho-03-06-06.jpg'>
												<img src='wp-content/uploads/2022/08/dong-tho-1.jpg'>
												<img src='wp-content/uploads/2022/08/dong-tho-03-03.jpg'>
												<img src='wp-content/uploads/2022/08/dong-tho-03-04.jpg'>
												<img src='wp-content/uploads/2022/08/dong-tho-03-05.jpg'>
												<img src='wp-content/uploads/2022/08/dong-tho-03-07.jpg'>
												<img src='wp-content/uploads/2022/08/dong-tho-3.jpg'>
										</div>
			</div>
			
		</div>
	</div>
</div>
<div class='sc_home-baochi'>
	<div class="container">
		<div class='sc_home-gallery'>
			<h2 class='text-center fw-600 fs-40 mg-b-30'>
				<span class='hLine hLineS hLineSs'>Báo chí nói về SBS</span>
			</h2>
							<div class='sc_home-gallery-items gallery-event'>
											<a rel="nofollow" target='_blank' class='item' data-src='' href="https://www.24h.com.vn/bat-dong-san/cap-nhat-xu-huong-nha-pho-xanh-cung-sbs-house-c792a1276332.html">
					    	<img src="wp-content/uploads/2022/08/24h-01.png">
					    </a>
												<a rel="nofollow" target='_blank' class='item' data-src='' href="https://dantri.com.vn/doi-song/can-nha-khong-mai-mat-quanh-nam-cua-vo-chong-da-nang-20221223112557018.htm">
					    	<img src="wp-content/uploads/2023/02/dantri-1.png">
					    </a>
												<a rel="nofollow" target='_blank' class='item' data-src='' href="https://kienviet.net/2022/04/28/khong-gian-song-goi-mo-nguon-cam-hung-bat-tan-tu-phong-cach-modern-classic/">
					    	<img src="wp-content/uploads/2022/08/kienviet-01.png">
					    </a>
												<a rel="nofollow" target='_blank' class='item' data-src='' href="https://vtv.vn/goc-doanh-nghiep/dieu-gi-khien-sbs-house-tro-thanh-don-vi-thiet-ke-va-thi-cong-nha-o-hang-dau-da-nang-20210802112923984.htm">
					    	<img src="wp-content/uploads/2022/08/vtv-01.png">
					    </a>
												<a rel="nofollow" target='_blank' class='item' data-src='' href="https://cafef.vn/sbs-house-thiet-ke-va-thi-cong-nha-pho-biet-thu-uy-tin-2021072611510018.chn">
					    	<img src="wp-content/uploads/2022/08/cafef-01.png">
					    </a>
												<a rel="nofollow" target='_blank' class='item' data-src='' href="https://cafebiz.vn/sbs-house-cong-ty-thiet-ke-va-thi-cong-uy-tin-tai-da-nang-20210625133647502.chn">
					    	<img src="wp-content/uploads/2022/08/cafebiz-01.png">
					    </a>
												<a rel="nofollow" target='_blank' class='item' data-src='' href="https://vnexpress.net/ngoi-nha-che-khien-4559690.html">
					    	<img src="wp-content/uploads/2022/08/vnexpress-01.png">
					    </a>
												<a rel="nofollow" target='_blank' class='item' data-src='' href="https://happynest.vn/chuyen-nha/5212/nha-pho-2-tang-da-nang-don-gian-de-ung-dung-phu-hop-voi-nhu-cau-cua-gia-dinh-tre-3-nguoi">
					    	<img src="wp-content/uploads/2022/08/happynest-01.png">
					    </a>
												<a rel="nofollow" target='_blank' class='item' data-src='' href="https://www.doisongphapluat.com/thie-t-ke-nha-online-tu-xa-cu-ng-sbs-house-gia-i-pha-p-to-i-uu-cho-mu-a-di-ch-a511008.html">
					    	<img src="wp-content/uploads/2022/08/doisongphapluat-01.png">
					    </a>
												<a rel="nofollow" target='_blank' class='item' data-src='' href="https://baoquangnam.vn/ban-can-biet/mau-nha-pho-ong-hien-dai-xuat-hien-ngay-cang-nhieu-o-khu-do-thi-moi-120501.html">
					    	<img src="wp-content/uploads/2022/08/quangnam-01.png">
					    </a>
												<a rel="nofollow" target='_blank' class='item' data-src='' href="https://baodanang.vn/can-biet/202110/xu-huong-thiet-ke-nha-da-nang-hau-dai-dich-3892822/">
					    	<img src="wp-content/uploads/2022/08/danang-01.png">
					    </a>
												<a rel="nofollow" target='_blank' class='item' data-src='' href="https://baoquangngai.vn/channel/8181/202112/xu-huong-nha-pho-tao-nen-dien-mao-tre-trung-nang-dong-cho-quang-ngai-3093143/">
					    	<img src="wp-content/uploads/2022/08/quangngai-01.png">
					    </a>
												<a rel="nofollow" target='_blank' class='item' data-src='' href="https://baothuathienhue.vn/nha-pho-theo-phong-cach-hien-dai-tai-hue-a108064.html">
					    	<img src="wp-content/uploads/2022/08/baohue-01-e1660637844893.png">
					    </a>
												<a rel="nofollow" target='_blank' class='item' data-src='' href="https://vnmedia.vn/kinh-te/thi-truong/202106/mot-so-mau-nha-dep-hien-nay-9be70ad/">
					    	<img src="wp-content/uploads/2022/08/vnmedia-01.png">
					    </a>
												<a rel="nofollow" target='_blank' class='item' data-src='' href="https://plo.vn/nha-pho-mang-phong-cach-hien-dai-va-toi-gian-post611174.html">
					    	<img src="wp-content/uploads/2022/08/phapluat-01.png">
					    </a>
												<a rel="nofollow" target='_blank' class='item' data-src='' href="https://afamily.vn/ngoi-nha-100m-thiet-ke-thong-minh-tien-loi-danh-cho-gia-dinh-3-the-he-o-da-nang-20200214151132349.chn">
					    	<img src="wp-content/uploads/2022/08/afamily-01.png">
					    </a>
										</div>
							</div>
		</div>
	</div>
</div>

<div class='sc_home-customer'>
	<div class="container">
		<h3 class='cl fw-600 fs-30 mg-b-20 text-center'>
			<span class='hLine hLineS'>Khách hàng nói gì về SBS</span>
		</h3>
		<div class='sc_slide hasdot' items='3' items-tablet='2' items-mobile='1' margin="50">
					            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Bản vẽ thiết kế kết cấu rất chi tiết và thuận lợi trong quá trình giám sát, không gian, nội thất căn nhà rất đầy đủ ánh sáng, thoáng mát ...</div>
							<div class='text-right mg-t-20'><a href='khach-hang-hoang-pham-ngoc-thach/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/10/KH-Thach-BMT.jpg'>
							<div class=''>
								<div class='cl fw-500'>Hoàng Phạm Ngọc Thạch</div>
								<div class='cl5 fs-12'>Kỹ sư</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Mai em về nhà mới ạ! Rất tâm đắc, cảm ơn SBS nhiều, chuẩn bị tất toán thôi cho SBS thôi nào !</div>
							<div class='text-right mg-t-20'><a href='khach-hang-ngo-ky-duyen/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/10/KH-ky-duyen-quang-ngai.jpg'>
							<div class=''>
								<div class='cl fw-500'>Ngô Kỳ Duyên</div>
								<div class='cl5 fs-12'></div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Rất ưng í ngoài thiết kế còn nhiệt tình, trước cũng có liên hệ vài đơn vị nhưng họ không nhiệt tình nên rất khó làm việc, may là gặp SBS.</div>
							<div class='text-right mg-t-20'><a href='khach-hang-nguyen-dinh-phong/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/09/KH-Nguyen-Dinh-Phong.jpg'>
							<div class=''>
								<div class='cl fw-500'>Nguyễn Đình Phong</div>
								<div class='cl5 fs-12'>Cán bộ ngân hàng</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Nhà em tiếp khách xem thiết kế nhiều quá anh Trung (SBS) nghe. Thiết kế bớt đẹp lại đi hỉ ! Giờ chắc chỉ cho khách đứng ngoài chụp ảnh thôi 🤣</div>
							<div class='text-right mg-t-20'><a href='khach-hang-nhu-y-vo-anh-thuan/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/09/KH-Vo-THuan-Tam-Ky.jpg'>
							<div class=''>
								<div class='cl fw-500'>Ms Như Ý (vợ anh Thuận)</div>
								<div class='cl5 fs-12'>Quản lý thị trường</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Là người kỹ tính, cầu toàn, yêu cầu chỉnh từng ly từng chút nhưng Team SBS luôn lắng nghe và tôn trọn ý kiến điều chỉnh cho hợp lý nhất.</div>
							<div class='text-right mg-t-20'><a href='kh-binh/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/KH-Binh.jpg'>
							<div class=''>
								<div class='cl fw-500'>Anh Bình</div>
								<div class='cl5 fs-12'>Cán bộ nhà nước</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Đầu tiên anh phải nói là vợ chồng anh rất thích bản thiết kế này, mấy đứa nhỏ cũng thích. Cả nhà coi từ chiều đến giờ 😄</div>
							<div class='text-right mg-t-20'><a href='index90ef.html?p=26048&amp;preview=true'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/KH-HD-76.jpg'>
							<div class=''>
								<div class='cl fw-500'>Lê Lộc</div>
								<div class='cl5 fs-12'>Kỹ sư</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Chưa nghĩ là mình làm nhà bây giờ, vì mình mới xây cách đây 5 năm. Hôm trước thấy SBS thiết kế nhà đẹp và hợp ý mình nên chốt luôn.</div>
							<div class='text-right mg-t-20'><a href='kh-nguyen-huong-giang/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/Giang-vo-Thanh-HD-69.jpg'>
							<div class=''>
								<div class='cl fw-500'>Nguyễn Hương Giang</div>
								<div class='cl5 fs-12'>Kinh doanh</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Thiết kế xuất sắc, đúng ý và hợp gu của chị. 
Cảm ơn em nhiều nhé!
Chúc SBS ngày càng thành công, tạo lại nhiều sản phẩm chất lượng.</div>
							<div class='text-right mg-t-20'><a href='kh-ms-thao-hoi-an/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/KH-HD-115.jpg'>
							<div class=''>
								<div class='cl fw-500'>Chị Thảo</div>
								<div class='cl5 fs-12'>Kế toán</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>SBS thiết kế cho em cái nhà quá ưng, mà ở nhà thoáng mát lắm, gió lưu thông tốt, góc nào cũng thoáng luôn, em thích nhất cái đó!</div>
							<div class='text-right mg-t-20'><a href='kh-ta-tuan-anh/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/KH-Ta-Tuan-ANh.jpg'>
							<div class=''>
								<div class='cl fw-500'>Tạ Tuấn Anh</div>
								<div class='cl5 fs-12'>Kỹ sư IT</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Thanks mấy anh em SBS HOUSE đã giúp chị thiết kế và thi công hoàn thiện căn nhà này! Ưng bụng lắm!</div>
							<div class='text-right mg-t-20'><a href='kh-phan-thu-vy/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/KH-Phan-Thu-Vy.jpg'>
							<div class=''>
								<div class='cl fw-500'>Phạm Thu Vy</div>
								<div class='cl5 fs-12'>Kinh doanh</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Cảm ơn SBS HOUSE rất nhiều! Sự chuyên nghiệp, uy tín và phong cách độc đáo, hiện đại của các bạn đã mang cho tội sự hài lòng.


</div>
							<div class='text-right mg-t-20'><a href='khach-hang-nguyen-dac-cuong/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/KH-Nguyen-Dac-Cuong.jpg'>
							<div class=''>
								<div class='cl fw-500'>Nguyễn Đắc Cường</div>
								<div class='cl5 fs-12'>Kỹ thuật viên FPT Telecom</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Quá ưng, ai cũng khen rồi dừng lại chụp hình, đậu xe lại hỏi thiết kế ở đâu hoài!</div>
							<div class='text-right mg-t-20'><a href='kh-nhu-y/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/KH-Nhu-Y.jpg'>
							<div class=''>
								<div class='cl fw-500'>Như Ý</div>
								<div class='cl5 fs-12'>Luật sư</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Mục tiêu 30 tuổi của vợ chồng em đã thành hiện thực. Đặc biệt gửi lời cảm ơn chân thành đến SBS HOUSE một đơn vị làm việc có TÂM có TẦM</div>
							<div class='text-right mg-t-20'><a href='khach-hang-hoang-phuong/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/KH-Tham-Phuong.jpg'>
							<div class=''>
								<div class='cl fw-500'>Hoàng Phương</div>
								<div class='cl5 fs-12'>Kỹ sư</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Sau bao năm tần tảo cố gắng cuối cùng thành quả của vợ chồng em là tậu được một căn nhà bé bé xinh xinh. </div>
							<div class='text-right mg-t-20'><a href='khach-hang-vo-phuong-diem/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/KH-Diem-Son-hoa-quy.jpg'>
							<div class=''>
								<div class='cl fw-500'>Võ Phương Diễm</div>
								<div class='cl5 fs-12'>Kinh doanh</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Góc khoe nhà! Thật sự rất ưng ý :)) SBS HOUSE đội ngũ kỹ sư trẻ thiết kế và thi công rất tâm huyết, trình độ chuyên môn cao.</div>
							<div class='text-right mg-t-20'><a href='khach-hang-nguyen-trong-quang/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/KH-Nguyen-Trong-Quang.jpg'>
							<div class=''>
								<div class='cl fw-500'>Nguyễn Trọng Quang</div>
								<div class='cl5 fs-12'>Kỹ sư cầu đường</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>My house ❤️ 
Loading…………
Mọi người để em thử nghiệm trước. Nếu xây hoàn thiện hoàn toàn, mà ngon oki thì em review cho mọi người nhé</div>
							<div class='text-right mg-t-20'><a href='khach-hang-doan-nam-ny/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/KH-Nam-Ny-Thanh-binh-phuoc.jpg'>
							<div class=''>
								<div class='cl fw-500'>Đoàn Nam Ny</div>
								<div class='cl5 fs-12'>Kinh doanh</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Toàn bộ quá trình từ thiết kế đến xây dựng thì em rất hài lòng với SBS. Đặc biệt về việc hỗ trợ giải quyết các vấn đề phát sinh ...</div>
							<div class='text-right mg-t-20'><a href='khach-hang-nguyen-giang/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/KH-Giang-Lien-Chieu.jpg'>
							<div class=''>
								<div class='cl fw-500'>Nguyễn Giang</div>
								<div class='cl5 fs-12'>Chuyên viên Marketing</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Dù thời gian thi công bị ngắt quãng bởi covid nhưng với sự nổ lực và quyết tâm của ae SBS ngôi nhà đã được hoàn thiện chính xác, tỉ mỉ.</div>
							<div class='text-right mg-t-20'><a href='khach-hang-mr-trung/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/KH-Mr-Trung.jpg'>
							<div class=''>
								<div class='cl fw-500'>Anh Trung</div>
								<div class='cl5 fs-12'>Cán bộ ngân hàng</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Phải công nhận bên SBS thiết kế rất ưng ý, nhanh gọn và uy tín. Chúc SBS thành công và có nhiều dự án hơn nữa.</div>
							<div class='text-right mg-t-20'><a href='khach-hang-tuan-anh/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/KH-Tuan-Anh.jpg'>
							<div class=''>
								<div class='cl fw-500'>Tuấn Anh</div>
								<div class='cl5 fs-12'>Kỹ sư</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Nhờ thiết kế đẹp, nhà nhỏ mà ai cũng tưởng 5x20m không à! Thi công còn đẹp hơn cả thiết kế đấy !</div>
							<div class='text-right mg-t-20'><a href='khach-hang-ms-thao/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/KH-Thao-Dong-Nai.jpg'>
							<div class=''>
								<div class='cl fw-500'>Chị Thảo</div>
								<div class='cl5 fs-12'>Bác sỹ</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Gia đình nhỏ xin gửi lời cảm ơn đến SBS đã giúp đỡ em rất nhiều trong suốt thời gian qua để ngôi nhà hoàn hảo trong sự khen ngợi của mọi người!</div>
							<div class='text-right mg-t-20'><a href='kh-hanh-nguyen/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/08/KH-Hanh-Nguyen.jpg'>
							<div class=''>
								<div class='cl fw-500'>Nguyễn Hạnh</div>
								<div class='cl5 fs-12'>Giáo viên</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Xin cảm ơn tất cả các ace SBS House đã hoàn thiện bản thiết kế cho gia đình em!</div>
							<div class='text-right mg-t-20'><a href='khach-hang-nguyen-son/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/09/KH-Nguyen-Son.jpg'>
							<div class=''>
								<div class='cl fw-500'>Nguyễn Sơn</div>
								<div class='cl5 fs-12'>Kinh doanh</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Anh chân thành cảm ơn công ty SBS và các bạn tham gia thiết kế cho nhà anh nhé! Nhà anh sang năm mới làm anh sẽ liên hệ lại SBS thi công nhé!</div>
							<div class='text-right mg-t-20'><a href='khach-hang-huynh-nhat-quang/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/09/KH-Quang-HD12.jpg'>
							<div class=''>
								<div class='cl fw-500'>Huỳnh Nhật Quang</div>
								<div class='cl5 fs-12'>Kỹ sư</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Gia đình, bạn bè, người thân xem nhà ưng ý lắm luôn ạ. Cảm ơn team trong suốt thời gian qua đã giúp đỡ hoàn thiện nhà em thật xuất sắc.</div>
							<div class='text-right mg-t-20'><a href='khach-hang-kim-chau/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/09/KH-Kim-Chau-1.jpg'>
							<div class=''>
								<div class='cl fw-500'>Chồng chị Kim Châu</div>
								<div class='cl5 fs-12'>Giảng viên</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Xin chân thành cảm ơn sâu sắc tới đội ngũ bên phía cty thiết kế SBS đã đồng hành và chia sẻ mọi vấn đề với gia đình mình!</div>
							<div class='text-right mg-t-20'><a href='khach-hang-ms-van-vo-anh-phuc/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/09/KH-HD-117.jpg'>
							<div class=''>
								<div class='cl fw-500'>Ms Vân (vợ anh Phúc)</div>
								<div class='cl5 fs-12'>Kế toán</div>
							</div>
						</div>
					</div>
		            		            <div class='sc_home-customer-item'>
						<div class='sc_home-customer-content'>
							<div><i class="sc_home-customer-quote fa fa-quote-left"></i></div>
							<div>Cảm ơn các anh đã thiết kế và thi công cho vợ chồng em căn nhà rất ưng ý. Chất lượng thì không cần phải đề cập vì đã tin tưởng làm công trình thứ 3 rồi! </div>
							<div class='text-right mg-t-20'><a href='khach-hang-van-anh/index.html'>Xem thêm </a></div>
						</div>
						<div class='sc_home-customer-intro'>
							<img src='wp-content/uploads/2022/09/KH-Van-Anh.jpg'>
							<div class=''>
								<div class='cl fw-500'>Ms Vân Anh</div>
								<div class='cl5 fs-12'>Kinh doanh</div>
							</div>
						</div>
					</div>
		            		</div>
	</div>
</div>
<div id='sc_contact_box' class='sc_contact'>
	<div class="container">
		<h3 class='text-center cl fw-600 fs-30 mg-b-20'>
			<span class='hLine hLineS'>Liên hệ với chúng tôi</span>
		</h3>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<form class='ajax_form_content' action="https://sbshouse.vn/form_contact">
					<div class='sc_box-input'>
						<div class='s_label'>Họ và tên</div>
						<input type='input' required="true" name='fullname'>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class='sc_box-input'>
								<div class='s_label'>Địa chỉ email</div>
								<input type='email' required="true" name='email'>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class='sc_box-input'>
								<div class='s_label'>Số điện thoại</div>
								<input type='tel' required="true" name='phone'>
							</div>
						</div>
					</div>
					<div class='sc_box-input'>
						<div class='s_label'>Lời nhắn</div>
						<textarea required="true" name='mes' placeholder="Ngân sách, diện tích, số tầng..."></textarea>
					</div>
					<div class='mobile_center'>
						<input type="hidden" name="main-url" value="index.html">
						<button class='btnSite_form' type='submit'>Gửi cho chúng tôi <i class="fa fa-envelope-o"></i></button>
					</div>
				</form>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 sc_contact-img">
				<img class='sc-rimg' src='wp-content/uploads/2022/10/chi-ha-footer.png'>
				<div class='box_icon'>
					<a target='_blank' href='https://www.facebook.com/sbshouse.vn'><img src='wp-content/themes/sbshouse/images/fb.png'></a>
					<a target='_blank' href='https://www.youtube.com/c/sbshouse'><img src='wp-content/themes/sbshouse/images/yt.png'></a>
					<a target='_blank' href='https://www.tiktok.com/@sbshouse_nhadep'><img src='wp-content/themes/sbshouse/images/ar.png'></a>
				</div>
			</div>
		</div>
	</div>
</div><footer id="sc_footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
				<div class='mg-t-70'>
					<img src="wp-content/uploads/2022/07/logo.svg" width="100">
				</div>
			</div>
			<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<div class="sc_footer-title">CÔNG TY TNHH THIẾT KẾ VÀ XÂY DỰNG SBS</div>
						<div class="sc_footer-text sc_footer-text-intro">
							<p>SBS HOUSE cung cấp dịch vụ thiết kế, thi công nhà đẹp tại Đà Nẵng. Hoàn thiện nội thất nhà thô, căn hộ…</p>
						</div>
						<div class="sc_footer-title" style="border: 0;margin-top: 0;padding-top: 15px;">DỊCH VỤ</div>
							<ul class="sc_footer_menu">
																            <li><a href='chuyen-muc/nha-pho-2-tang/index.html'>Nhà phố 2 tầng</a></li>
								            								            <li><a href='chuyen-muc/nha-pho-3-tang/index.html'>Nhà phố 3 tầng</a></li>
								            								            <li><a href='chuyen-muc/nha-pho-4-tang/index.html'>Nhà phố 4 tầng</a></li>
								            								            <li><a href='chuyen-muc/biet-thu-nha-vuon-2/index.html'>Village</a></li>
								            							</ul>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<div class="sc_footer-title">LIÊN HỆ</div>
						<div class="sc_footer-text"><p>0972 910 046</p>
<p>Trụ sở chính: 284-286 Lê Quảng Chí, Đà Nẵng</p>
<p>Chi nhánh HCM:  72 Nguyễn Thị Nhung, Thủ Đức</p>
<p>Chi nhánh QT: 162 Lê Lợi, Đông Hà, Quảng Trị</p>
<p>Showroom 1: 201 Nguyễn Hữu Thọ, Đà Nẵng</p>
<p>Showroom 2: 526 Điện Biên Phủ, Đà Nẵng</p>
<p>Showroom 3: 51 Lê Duẩn, Đông Hà, Quảng Trị</p>
<p>Nhà xưởng 1: Khu công nghiệp Hòa Cầm, Đà Nẵng</p>
<p>Nhà xưởng 2: 119 Thanh Lương 16, Đà Nẵng</p>
</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<div class="sc_footer-title">FOLLOW SBSHOUSE</div>
						<div>
							<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="../connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v16.0" nonce="MOUabROS"></script>
<div data-lazy="true" class="fb-page" data-href="https://www.facebook.com/sbshouse.vn" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/sbshouse.vn" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/sbshouse.vn">SBS HOUSE</a></blockquote></div>						</div>
						<div class='mg-t-30'>
							<div class="sc_footer-text sc_footer-text-intro ">
								<p><b>Đơn vị tin cậy:</b> </p> <p> <span style="color: #ffffff;"><a style="color: #ffffff;" href="https://brocanvas.com/">BROCANVAS</a></span></p>   <p> <span style="color: #ffffff;"><a style="color: #ffffff;" href="https://sbsdoor.vn/">SBS DOOR</a></span></p>   <p> <span style="color: #ffffff;"><a style="color: #ffffff;" href="https://cmtgarden.com/">CMT GARDEN</a></span></p> 							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="sc_footer-copyright">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 hidden-xs">
									</div>
				<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
					<div class="row">
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							© Copyright 2014-2023. Bản quyền nội dung thuộc SBS HOUSE.						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 sc_footer-social hidden-xs">
							<a rel="nofollow" target='_blank' href='https://www.facebook.com/sbshouse.vn'><img src='wp-content/themes/sbshouse/images/fb1.png'></a>
							<a rel="nofollow" target='_blank' href='https://www.youtube.com/c/sbshouse'><img src='wp-content/themes/sbshouse/images/yt1.png'></a>
							<a rel="nofollow" target='_blank' href='https://www.tiktok.com/@sbshouse_nhadep'><img src='wp-content/themes/sbshouse/images/ar1.png'></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<div class="tld_chat_box tld_popup_fixed">
	<a class='tld_chat_show tld_open_show'><img src="wp-content/themes/sbshouse/images/im1.png"></a>
	<div class='tld_chat_box_content tld_popup_content'>
		<div class='text-right'><a class='tld_chat_close tld_close_popup'><img src="wp-content/themes/sbshouse/images/clsclose.png"></a></div>
		<div class='tld_chat tld_popup_body'>
			<div class='title'>Bạn muốn tư vấn ở đâu?</div>
			<div class='items'>
				<a class='fb' href='https://m.me/1863538543903301'>
					<div class='clc_b'>Nhắn tin qua Facebook</div>
				</a>
				<a class='zl' href='https://zalo.me/0973222166'>
					<div class='clc_b'>Nhắn tin qua Zalo</div>
				</a>
				<a class='call desktop_hidden' href='tel:0972910046'>
					<div class='clc_b'>Gọi điện thoại trực tiếp</div>
				</a>
				<a class='chat' href="#">
					<div class='clc_b'>Tư vấn tại đây</div>
				</a>
			</div>
		</div>
	</div>
</div><div class="tld_faq_box tld_popup_fixed">
	<a class='tld_faq_show tld_open_show'><img src="wp-content/themes/sbshouse/images/icon-faq.png"></a>
	<div class='tld_faq_box_content tld_popup_content'>
		<div class='text-right'><a class='tld_faq_close tld_close_popup'><img src="wp-content/themes/sbshouse/images/clsclose.png"></a></div>
		<div class='tld_faq_body tld_popup_body'>
			<div class='tld_faq-top-title'>
				<form class='tld-popup-faq-search' action="https://sbshouse.vn/faqSearch">
					<input name="keyword" type="text" placeholder="Chào bạn, SBS rất vui giải đáp các thắc mắc từ bạn!">
	                <button><img src='wp-content/themes/sbshouse/images/icon-search-faq.png'></button>
				</form>
			</div>
			<div class='tld_popup-faq-items'>
				<div class='boxFA'>
							                        <div class='item'>
		                            <div class='title'>
		                                Cầu thang hướng ra cửa chính và từ cửa chính hướng vào bếp có phạm phong thủy không?		                                <i class="fa fa-angle-down"></i>
		                            </div>
		                            <div class='content'>
		                            	Theo quan niệm dân gian xưa, cầu thang hướng ra cửa, hay bếp hướng ra cửa chính sẽ phạm phong...		                            	<div class='text-right'><a href='faq/cau-thang-huong-ra-cua-chinh-va-tu-cua-chinh-huong-vao-bep-co-pham-phong-thuy-khong/index.html' class='rmore'>Xem thêm</a></div>
		                            </div>
		                        </div>
		                        		                        <div class='item'>
		                            <div class='title'>
		                                Nhà thầu thi công không giống với thiết kế?		                                <i class="fa fa-angle-down"></i>
		                            </div>
		                            <div class='content'>
		                            	Khi lựa chọn dịch vụ thiết kế kiến trúc của SBS HOUSE, chủ đầu tư hoàn toàn yên tâm về...		                            	<div class='text-right'><a href='faq/nha-thau-thi-cong-khong-giong-voi-thiet-ke/index.html' class='rmore'>Xem thêm</a></div>
		                            </div>
		                        </div>
		                        		                        <div class='item'>
		                            <div class='title'>
		                                Hồ sơ xây dựng nhà hoàn chỉnh gồm những gì?		                                <i class="fa fa-angle-down"></i>
		                            </div>
		                            <div class='content'>
		                            	Hồ sơ hoàn chỉnh khi xây nhà gồm phần kiến trúc, kết cấu, hạng mục điện, nước, nội thất và...		                            	<div class='text-right'><a href='faq/ho-so-xay-dung-nha-hoan-chinh-gom-nhung-gi/index.html' class='rmore'>Xem thêm</a></div>
		                            </div>
		                        </div>
		                        		                        <div class='item'>
		                            <div class='title'>
		                                Chất liệu làm nội thất giường, tủ và tủ bếp		                                <i class="fa fa-angle-down"></i>
		                            </div>
		                            <div class='content'>
		                            	Tủ bếp, giường ngủ và tủ kệ tivi,... là những món đồ nội thất quan trọng trong nhà, chúng ta...		                            	<div class='text-right'><a href='faq/chat-lieu-lam-noi-that-giuong-tu-va-tu-bep/index.html' class='rmore'>Xem thêm</a></div>
		                            </div>
		                        </div>
		                        		        </div>
	            <div class='text-center mg-b-5'><a href='faqs/index.html'>Xem tất cả</a></div>
			</div>
		</div>
	</div>
</div><a class='scroll_top'><i class="fa fa-angle-up"></i></a>
<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/swv/js/indexf2b4.js?ver=5.7.7' id='swv-js'></script>
<script type='text/javascript' id='contact-form-7-js-extra'>
/* <![CDATA[ */
var wpcf7 = {"api":{"root":"https:\/\/sbshouse.vn\/wp-json\/","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/indexf2b4.js?ver=5.7.7' id='contact-form-7-js'></script>
<script type='text/javascript' id='kk-star-ratings-js-extra'>
/* <![CDATA[ */
var kk_star_ratings = {"action":"kk-star-ratings","endpoint":"https:\/\/sbshouse.vn\/wp-admin\/admin-ajax.php","nonce":"d3098e87da"};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/kk-star-ratings/src/core/public/js/kk-star-ratings.min5243.js?ver=5.4.5' id='kk-star-ratings-js'></script>
<script type='text/javascript' src='wp-content/plugins/3d-flipbook-dflip-lite/assets/js/dflip.min3023.js?ver=1.7.35' id='dflip-script-js'></script>
</body>

<!-- Mirrored from sbshouse.vn/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Aug 2023 14:14:28 GMT -->
</html>


