<?php
function mir_add_css_js()
{
    wp_enqueue_script('jquery');
    /* Add fontawesome */
    // wp_enqueue_script( 'fontawesome-js', 'https://kit.fontawesome.com/30559742c7.js', array(), '1.0', false );
    wp_enqueue_style( "font-awsome", "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.0/css/all.min.css");
    wp_enqueue_style( "font-awsome-pro", "https://pro.fontawesome.com/releases/v5.10.0/css/all.css?version=20210319224100");
    // Add Css
    wp_enqueue_style('animate',get_stylesheet_directory_uri() .'/assets/css/animate.mind58c.css',array(), time());
    wp_enqueue_style('bootstrap',get_stylesheet_directory_uri() .'/assets/css/bootstrap.mind58c.css',array(), time());
    wp_enqueue_style('defaultd',get_stylesheet_directory_uri() .'/assets/css/defaultd58c.css',array(), time());
    wp_enqueue_style('carousel',get_stylesheet_directory_uri() .'/assets/css/owl.carousel3781.css',array(), time());
    wp_enqueue_style('themed58c',get_stylesheet_directory_uri() .'/assets/css/themed58c.css',array(), time());
    wp_enqueue_style('responsived58c',get_stylesheet_directory_uri() .'/assets/css/responsived58c.css',array(), time());
    wp_enqueue_style('main_css',get_stylesheet_directory_uri() .'/assets/css/main.css',array(), time());
    wp_enqueue_style('res_css',get_stylesheet_directory_uri() .'/assets/css/responsive.css',array(), time());
    wp_enqueue_style('pluss_css',get_stylesheet_directory_uri() .'/assets/css/pluss.css',array(), time());
    wp_enqueue_style('woocommerce',get_stylesheet_directory_uri() .'/assets/css/woocommerce.css',array(), time());
    wp_enqueue_style('talent_main_css',get_stylesheet_directory_uri() .'/assets/css/talent-main.css',array(), time());
    // if($lang_current == 'en'){
        wp_enqueue_style('style_en_css',get_stylesheet_directory_uri() .'/assets/css/style-en.css',array(), time());
    // }
    // Add js
    wp_enqueue_script( 'main_js', get_stylesheet_directory_uri() . '/assets/js/maind58c.js', array(), time() );
    wp_enqueue_script( 'wookmark', get_stylesheet_directory_uri() . '/assets/js/jquery.wookmark.mind58c.js', array(), time());
    wp_enqueue_script( 'validate_form',"https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js", array(), time());
    wp_enqueue_script( 'owl_carousel', get_stylesheet_directory_uri() . '/assets/js/owl.carousel.mind58c.js', array(), time() );
    wp_enqueue_script( 'sticky_js', get_stylesheet_directory_uri() . '/assets/js/jquery.sticky.js', array(), time() );
    wp_enqueue_script( 'waypoints_js', get_stylesheet_directory_uri() . '/assets/js/jquery.waypoints.js', array(), time() );

    wp_enqueue_script( 'slidebars_js', get_stylesheet_directory_uri() . '/assets/js/slidebars.js', array(), time());
    wp_enqueue_script( 'swiper_js', get_stylesheet_directory_uri() . '/assets/js/swiper.jquery.js', array(), time());

    wp_enqueue_script( 'inview_js', get_stylesheet_directory_uri() . '/assets/js/inview.js', array(), time());
    if(is_product()){
    wp_enqueue_script( 'jquery-ui', get_stylesheet_directory_uri() . '/assets/js/jquery-ui.js', array(), time());
    }
    wp_enqueue_script( 'wc-cart-fragments' );
    wp_localize_script( 'main_js', 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action('wp_enqueue_scripts','mir_add_css_js');