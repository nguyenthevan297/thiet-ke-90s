var _hwindown = jQuery(window).outerHeight();
var _page_archive = 2;
var _loadSite = false;
jQuery(document).on("click touch",".sc_tabs-nav a",function(){
    var _this = jQuery(this);
    var _parent = _this.parents(".sc_tabs-nav-box");
    if(!_this.hasClass("active")){
        _parent.find(".sc_tabs-nav a").removeClass("active");
        _parent.find(".sc_tabs-body-item").removeClass("active");
        _this.addClass("active");
        jQuery(_this.attr("data-id")).addClass("active");
        window.dispatchEvent(new Event('resize'));
    }
});
jQuery(document).on("click touch",".trigger_search",function(){
    jQuery('.form_search').toggleClass("active");
});
jQuery(document).on("click touch",".tld_open_show",function(){
    jQuery('.tld_open_show').show();
    jQuery('.tld_popup_content').css({"display":"none"});
    jQuery('.tld_popup_fixed').removeClass("tld_popup_active");


    var _this = jQuery(this);
    var _parent = _this.closest(".tld_popup_fixed");
    _parent.addClass("tld_popup_active");
    _parent.find('.tld_open_show').css({"display":"none"});
    _parent.find('.tld_popup_content').show();
    return false;
});
jQuery(document).on("click touch",".tld_close_popup",function(){
    var _this = jQuery(this);
    var _parent = _this.closest(".tld_popup_fixed");
    _parent.removeClass("tld_popup_active");
    _parent.find('.tld_popup_content').css({"display":"none"});
    _parent.find('.tld_open_show').show();
    return false;
});
jQuery(document).on("click touch",".tld_chat .chat",function(){
    Tawk_API.showWidget();
    Tawk_API.maximize();
    jQuery('.tld_chat_box_content').css({"display":"none"});
    return false;
});
jQuery(document).on("click touch",".faqsBox .title,.tld_popup-faq-items .title",function(){
    var _this = jQuery(this);
    var _parent = _this.closest(".item");
    _parent.toggleClass("active");
    return false;
});
jQuery(document).on("click touch",".loadMore_archive",function(){
    var _this = jQuery(this);
    if(!_this.hasClass('loading')){
        _this.addClass('loading');
        var _catID = _this.attr("data-id");
        var _filterSlug = _this.attr("data-filter");
        var data = {
            action: "archiver_loadmore",
            catID: _catID,
            filterSlug:_filterSlug,
            pageNumber:_page_archive
        };
        jQuery.post(ajaxurl,data, function(html){
            jQuery('.col_archive_items').append(html);
            _page_archive = _page_archive+1;
            _this.removeClass('loading');
            if(html==""){
                _this.remove();
            }
        });
    }
    
    return false;
});
jQuery(document).on("submit",".tld-popup-faq-search",function(){
    if(_loadSite) return false;
    _loadSite = true;
    var _this = jQuery(this);
    var data = {
        action: _this.attr("action"),
        postdata: _this.serialize()
    };
    jQuery.post(ajaxurl,data, function(html){
        _loadSite = false;
        jQuery(".tld_popup-faq-items .boxFA").html(html);
    });
    return false;
});
jQuery(document).on("submit",".ajax_form_content",function(){
    var _this = jQuery(this);
    if(_this.hasClass("active")){
        return;
    }
    _this.addClass("active");
    _this.find("button").html('Đang gửi thông tin <i class="fa fa-refresh fa-spin"></i>');
    var data = {
        action: _this.attr("action"),
        postdata: _this.serialize()
    };
    jQuery.post(ajaxurl,data, function(html){
        _this.find("button").html("Đã gửi thành công <i class='fa fa-check'></i>");
    });
    return false;
});
jQuery(document).on("click touch",".trigger_menu,.trigger_menu2",function(){
    jQuery('.main_menu').toggleClass("active");
    jQuery('body').toggleClass("no-scroll");
});
jQuery(document).on("click",".scroll_top",function(){
    jQuery('body,html').animate({scrollTop:0});
    return false;
});
jQuery(document).ready(function(){
    if(jQuery('.box_video_full video').length > 0){
        jQuery('body').addClass('no-scroll');
        var _v = jQuery('.box_video_full video');
        //_v[0].playbackRate = 1.5;
        _v[0].play();
        /*_v[0].onended = function(){
            
        };
        _v[0].ontimeupdate = function(){
            
        };*/
    }


    jQuery("html").click(function(e){
        if(jQuery(e.target).closest('.form_search,.trigger_search').length === 0) {
            jQuery('.form_search').removeClass("active");
        }
    });

    if(jQuery('.content_more').length > 0){
        jQuery.each(jQuery(".content_more"),function(){
            var _this = jQuery(this);
            if(_this.outerHeight() > 1000){
                _this.append('<div class="content_more_box"><a class="content_more_btn">Xem thêm</a></div>').addClass('content_more_up');
            }
        });
    }
	jQuery(".has_slide_top").owlCarousel({
        loop: false,
        autoplay:true,
        items: 1,
        nav:true,
        stopOnHover : true,
        smartSpeed : 1500,
        autoplayTimeout: 5000,
        //navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-left"></i>'],
        touchDrag : true,
        mouseDrag: true,
        responsive: {
            320 :{
                items: 2
            },
            768 :{
                items: 4
            },
            1200 :{
                items: 6
            }
        }
    });
    if(jQuery(".sc_slide").length > 0){
        jQuery.each(jQuery('.sc_slide'),function(){
            var _this = jQuery(this);
            var _data = {
                items:1,
                responsive : {}
            };
            if(_this.attr('animateOut') && _this.attr('animateOut')!=''){
                _data.animateOut = _this.attr('animateOut');
            }
            if(_this.attr('loop') && _this.attr('loop')!='yes'){
                _data.loop = true;
            }
            if(_this.attr('autoplay') && _this.attr('autoplay')!='yes'){
                _data.autoplay = true;
            }
            if(_this.attr('nav')){
                _data.nav = true;
                _data.navText = ['<i class="fa fa-long-arrow-left"></i>','<i class="fa fa-long-arrow-right"></i>'];
                if(_this.attr('nav')!='yes'){
                    _data.navText = ['<i class="fa fa-long-arrow-left"></i>','<i class="fa fa-long-arrow-right"></i>'];
                    var _nav = _this.attr('nav').split("|");
                    if(_nav.length > 1){
                        _data.navText = [_nav[0],_nav[1]];
                    }  
                }
            }
            if(_this.attr('smartSpeed') && _this.attr('smartSpeed')!='yes'){
                _data.smartSpeed = parseInt(_this.attr('smartSpeed'));
            }
            if(_this.attr('autoplayTimeout') && _this.attr('autoplayTimeout')!='yes'){
                _data.autoplayTimeout = parseInt(_this.attr('autoplayTimeout'));
            }
            if(_this.attr('items') && _this.attr('items')!='yes'){
                _data.responsive[1170] = {items:parseInt(_this.attr('items'))};
            }
            if(_this.attr('items-tablet') && _this.attr('items-tablet')!='yes'){
                _data.responsive[768] = {items:parseInt(_this.attr('items-tablet'))};

            }
            if(_this.attr('items-mobile') && _this.attr('items-mobile')!='yes'){
                _data.responsive[320] = {items:parseInt(_this.attr('items-mobile'))};
            }
            if(_this.attr('margin') && _this.attr('margin')!='no'){
                _data.margin = parseInt(_this.attr('margin'));
            }
            _this.owlCarousel(_data);
        });
    }
});
jQuery(window).load(function(){
    setTimeout(function(){
        animate_site();
        set_animate_scroll();
    },400);
    _hwindown = jQuery(window).outerHeight();

}).resize(function(){
    _hwindown = jQuery(window).outerHeight();
});
function animate_site(){
    jQuery(window).scroll(function(){
        set_animate_scroll();
    });
}
function set_animate_scroll(){
    /*if(jQuery('.animation-box').length > 0){
        jQuery.each(jQuery('.animation-box'),function(){
            var _this = jQuery(this);
            var _off = _this.offset();
            if(jQuery(window).scrollTop() > (_off.top-(_hwindown/1.3))){
                _this.removeClass('animation-box').addClass(_this.attr('data-animate')+" animate__animated");
            }
        });
    }
    if(jQuery('.gallery-event').length > 0){
        jQuery.each(jQuery('.gallery-event'),function(){
            var _this = jQuery(this);
            var _off = _this.offset();
            //if(jQuery(window).scrollTop() > (_off.top-(_hwindown/1.4))){
                _this.removeClass('gallery-event');
                _this.find(".item").wookmark({
                    autoResize: true,
                    container: _this,
                    offset: 0,
                    outerOffset: 0
                });
            //}
        });
    }*/
    if(jQuery('.number_transtion').length > 0){
        jQuery.each(jQuery('.number_transtion'),function(){
            var _this = jQuery(this);
            var _off = _this.offset();
            var _n = parseInt(_this.attr('data-content')) || parseInt(_this.text());
            if(_n > 0){
                _this.html("");
            }
            if(jQuery(window).scrollTop() > (_off.top-(_hwindown/1.4))){
                _this.removeClass('number_transtion');
                var k = 0;
                _this.html(k);
                var _delay = 1500/_n;
                var n = setInterval(function(){
                    k = parseInt(k)+1;
                    _this.html(k);
                    if(k==_n) clearInterval(n);
                },_delay);
            }
        });
    }
}
(function($) {
    jQuery(document).on('click','.content_more_btn',function(){
        var _this = jQuery(this);
        _this.parents('.content_more_up').removeClass('content_more_up').find('.content_more_box').remove();
        return false;
    });
    jQuery(document).on('click touch','.tab_title',function(){
        var _this = jQuery(this);
        if(!_this.hasClass('active')){
            var _parent = _this.parents('.sc_tab_body');
            _parent.find('.tab_title,.sc_tab_content').removeClass('active');
            _this.addClass('active');
            _parent.find(_this.attr('href')).addClass('active');
        }
        return false;
    });
})(jQuery);